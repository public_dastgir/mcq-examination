-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2017 at 02:37 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apt`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_logs`
--

CREATE TABLE IF NOT EXISTS `data_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('info','debug','error','warning') NOT NULL DEFAULT 'info',
  `ip` varchar(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `message` varchar(256) NOT NULL,
  `log_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_verify`
--

CREATE TABLE IF NOT EXISTS `email_verify` (
  `id` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('info','debug','error','warning') NOT NULL DEFAULT 'info',
  `ip` varchar(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `message` varchar(256) NOT NULL,
  `log_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions_attempted`
--

CREATE TABLE IF NOT EXISTS `questions_attempted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utest_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_ticked` (`utest_id`,`question_id`,`user_id`,`choice`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions_main`
--

CREATE TABLE IF NOT EXISTS `questions_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(256) NOT NULL,
  `sub_category` varchar(256) NOT NULL,
  `direction` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_main`
-- REMOVED 73 Categories
--

-- --------------------------------------------------------

--
-- Table structure for table `questions_sub`
--

CREATE TABLE IF NOT EXISTS `questions_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `a` varchar(256) NOT NULL,
  `b` varchar(256) NOT NULL,
  `c` varchar(256) NOT NULL,
  `d` varchar(256) NOT NULL,
  `e` varchar(256) NOT NULL,
  `f` varchar(256) NOT NULL,
  `answer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_sub`
-- Removed 820 Questions and Answers
--

-- --------------------------------------------------------

--
-- Table structure for table `test_main`
--

CREATE TABLE IF NOT EXISTS `test_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `branch` enum('COMP','CIVI','MECH','ELEC','EXTC','BIOT') NOT NULL,
  `year` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_start` bigint(20) NOT NULL,
  `date_end` bigint(20) NOT NULL,
  `randomize` tinyint(1) NOT NULL,
  `added_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_questions`
--

CREATE TABLE IF NOT EXISTS `test_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `a` varchar(256) NOT NULL,
  `b` varchar(256) NOT NULL,
  `c` varchar(256) NOT NULL,
  `d` varchar(256) NOT NULL,
  `e` varchar(256) NOT NULL,
  `f` varchar(256) NOT NULL,
  `correct` int(11) NOT NULL DEFAULT '1',
  `cat_id` int(11) DEFAULT NULL,
  `category_question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`test_id`,`cat_id`,`category_question_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_student`
--

CREATE TABLE IF NOT EXISTS `test_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time_start` int(11) NOT NULL,
  `time_end` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `score` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `per_user_test` (`test_id`,`user_id`,`time_start`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fname` varchar(40) NOT NULL,
  `lname` varchar(40) NOT NULL,
  `branch` enum('COMP','CIVI','MECH','ELEC','EXTC','BIOT') NOT NULL,
  `year` int(11) NOT NULL DEFAULT '1',
  `email` varchar(50) NOT NULL,
  `uin` varchar(10) NOT NULL,
  `verified` int(11) NOT NULL DEFAULT '0',
  `role` enum('S','A') NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uin` (`uin`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
