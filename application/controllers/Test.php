<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	/**
	 * Constructor: Checks if User is validated.
	 * Also loads Test Model/Helper
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct();
		$this->check_isvalidated();
		$this->load->model('Test_Model');
	}

	/**
	 * Only Admin can access this controller functions
	 * @method check_isvalidated
	 */
	private function check_isvalidated()
	{
		if ($this->session->userdata('validated') != true) {
			redirect('sign_in');
		}
		if ($this->session->userdata('role') != 'A') {
			redirect('profile');
		}
	}

	/** Views */

	/**
	 * Add New Test Page
	 * @method add
	 */
	public function add()
	{
		$this->load->view('header', array('title' => 'Add Test'));
		$this->load->view('profile/main');
		$this->load->view('admin/test/content');
		$this->load->view('admin/sidebar');
		$this->load->view('footer');
	}

	/**
	 * Edit Test
	 * Provides Functionality to Add Questions,Edit Test Settings.
	 * Also provides small test statistics.
	 * @method edit
	 * @param  int	$id	Test ID
	 */
	public function edit($id)
	{
		$this->load->view('header', array('title' => 'Edit Test'));
		$this->load->view('profile/main');
		if (intval($id) > 0 && $this->Test_Model->exists($id)) {
			$this->load->view('admin/test/edit', [
							'id' => $id,
							'test' => $this->Test_Model->test_list($id),
							'questions' => $this->Test_Model->question_list($id),
							'studentTest' => $this->Test_Model->check_all_tests_student($id),
							'questionCategory' => $this->Test_Model->listQuestionCategories()
						]);
		} else
			$this->load->view('admin/test/content');
		$this->load->view('admin/sidebar');
		$this->load->view('footer');
	}

	/**
	 * List all Tests
	 * @method list_tests
	 */
	public function list_tests()
	{
		$this->load->view('header', array('title' => 'List Test'));
		$this->load->view('profile/main');
		$this->load->view('admin/test/list', ['tests' => $this->Test_Model->test_list()]);
		$this->load->view('admin/sidebar');
		$this->load->view('footer');
	}

	/**
	 * Shows the Statistics Page
	 * @method statistics
	 * @param  int	$id	TestId
	 */
	public function statistics($id)
	{
		$test = $this->Test_Model->test_list($id);
		if ($test == null) {
			redirect('profile');
		}
		$studentTest = $this->Test_Model->check_all_tests_student($id);
		for ($i = 0; $i < count($studentTest); $i++) {
			if ($studentTest[$i]->time_end <= time() && $studentTest[$i]->score == -1) {
				$retArray = $this->Test_Model->generateScore($studentTest[$i]->test_id, $studentTest[$i]->id, $studentTest[$i]->user_id);
				if ($retArray[0] == true) {
					$studentTest[$i]->score = $retArray[1];
				}
			}
		}
		$this->load->view('header', array('title' => 'Test Statistics'));
		$this->load->view('profile/main');
		$this->load->view('admin/test/stats', ['testId' => $id, 'tests' => $test, 'studentTest' => $studentTest]);
		$this->load->view('admin/sidebar');
		$this->load->view('footer');
	}


	/** Processing */

	/**
	 * Add New Test
	 * Only Inserts at SQL
	 * @method add_new_test
	 */
	public function add_new_test()
	{
		// Add Test Entry(Post Request contains information)
		$id = $this->Test_Model->add_test_entry();
		if ($id == 0)
			echo 'error';
		else
			echo $id;
		return;
	}


	/**
	 * Add New Question to Database
	 * @method add_new_question
	 * @param  int	$id	Test ID
	 */
	public function add_new_question($id)
	{
		$qArray = null;

		// Checks if test exists.
		if (intval($id) > 0 && $this->Test_Model->exists($id)) {
			$qArray = $this->Test_Model->add_question($id);
		}
		if ($qArray == null) {
			echo 'error';
		} else {
			echo json_encode($qArray);
		}
	}

	/**
	 * Removes Question from Database
	 * @method remove_question
	 * @param  int	$id	Question ID
	 */
	public function remove_question($id)
	{
		$retVal = 0;

		// Test should exist in Database.
		if (intval($id) > 0 && $this->Test_Model->question_exists($id, $this->session->userdata('id'))) {
			$retVal = $this->Test_Model->remove_question($id);
		}
		if ($retVal == 0) {
			echo 'error';
		} else {
			echo 'success';
		}
	}

	/**
	 * Save the Test Settings
	 * @method settings
	 * @param  int	$id	Test ID
	 */
	public function settings($id)
	{
		$retVal = $this->Test_Model->save_settings($id);
		if ($retVal == 0) {
			echo 'error';
		} else {
			echo 'success';
		}
		return;
	}

	/**
	 * Removes Test and it's Related Questions from Database
	 * @method remove
	 * @param  int	$id	Test ID
	 */
	public function remove($id)
	{
		$retVal = 0;

		if (intval($id) > 0 && $this->Test_Model->exists($id)) {
			$retVal = $this->Test_Model->remove($id);
		}
		if ($retVal == 0) {
			echo 'error';
		} else {
			echo 'success';
		}
	}

	/**
	 * Adds Questions to Test from specific question Category
	 * @method add_new_question_category
	 * @param  int	$id	TestId
	 */
	public function add_new_question_category($id)
	{
		// Get x Questions from Category
		$retVal = $this->Test_Model->getQuestionCategory($this->input->post('category'), $this->input->post('value'));
		if ($retVal == null) {
			echo 'error';
			return;
		}
		// Add Question to test $id.
		$retVal2 = $this->Test_Model->addQuestionCategory($id, $retVal);
		if ($retVal2[0] == -1) {
			echo 'error';
		} else {
			if ($retVal2[0] > 0) { // If contains duplicate, remove duplicaed
				for ($j = count($retVal2[1])-1; $j >= 0; $j--) {
					for ($i = count($retVal)-1; $i >= 0; $i--) {
						if ($retVal2[1][$j] == $retVal[$i]->id) {
							if (count($retVal) == 1) {
								$retVal = null;
							} else {
								array_splice($retVal, $i, $i); 
							}
							break;
						}
					}
				}
			}
			if ($retVal == null) {
				echo 'error_no_question';
			} else {
				echo json_encode($retVal);
			}
		}
	}
}
?>