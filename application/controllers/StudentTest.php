<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentTest extends CI_Controller {
	/**
	 * Constructor: Checks if User is validated.
	 * Also loads Test Model/Helper
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct();
		$this->check_isvalidated();
		$this->load->model('Test_Model');
		$this->load->library('Test_Helper');
	}

	/**
	 * Only Validated Users can view this class
	 * @method check_isvalidated
	 */
	private function check_isvalidated()
	{
		if ($this->session->userdata('validated') != true) {
			redirect('sign_in');
		}
	}

	/** Views */

	/**
	 * List all Tests (/Scores)
	 * @method list_tests
	 * @param  string	$type	Valid Types: ['test', 'score']
	 */
	public function list_tests($type)
	{
		// Load Header
		$this->load->view('header', array('title' => 'List Test'. (($type == 'score')?' Scores':'') ));
		$this->load->view('profile/main');
		// Get All Tests
		// 
		$tests = $this->Test_Model->test_list(0, $this->session->userdata('branch'), $this->session->userdata('year'));
		// If score, generate scores if not yet.
		if ($type == 'score') {
			for ($i = 0; $i < count($tests); $i++) {
				if (!$this->Test_Model->eligible($tests[$i]->id))
					continue;
				$testDetails = $this->Test_Model->check_all_tests($tests[$i]->id);
				if ($testDetails == null) {
					continue;
				}
				for ($j = 0; $j < count($testDetails); $j++) {
					if ($testDetails[$j]->score == -1) {
						$this->Test_Model->generateScore($tests[$i]->id, $testDetails[$j]->id, $this->session->userdata('id'));
					}
				}
			}
		}
		$this->load->view('profile/test/list', ['tests' => $tests, 'type' => $type]);
		$this->load->view('profile/sidebar');
		$this->load->view('footer');   
	}

	/**
	 * Starts a Test for current user
	 * @method takeTest
	 * @param  int		$testId		Test ID
	 * @param  bool		$newTest	true, if new test is given, for retake, false
	 */
	private function takeTest($testId, $newTest = true)
	{
		// Check if user can give $testId Test
		if (!$this->Test_Model->eligible($testId))
			redirect('test/list');

		$retArray = $this->Test_Model->start_test($testId, $newTest);

		// Cannot Generate Test due to some reason
		if ($retArray['status'] == 0) {
			redirect('test/list');
			return;
		}

		// Set Eligibility
		$this->test_helper->setEligible($retArray['id']);

		$test = $this->Test_Model->test_list($testId);

		switch($retArray['status']) {
			case 2: // Old Test is resumed, set the maxTime accordingly.
				$maxTime = $retArray['end'];
				break;
			default: // New Test
				// Get the Max time of test
				$maxTime = time()+($test[0]->time*60);
				break;
		}
		$this->test_helper->setMaxTime($maxTime);

		// Change url from 'retake' to 'take' for retakes
		if ($newTest == false) {
			redirect('test/take/'. $testId);
			return;
		}

		$this->load->view('header', array('title' => 'Test'));
		$this->load->view('profile/main');
		$this->load->view('profile/test/exam', ['new' => true, 'questions' => $this->Test_Model->question_list($testId, (intval($test[0]->randomize) == 1)?true:false), 'utId' => $retArray['id']]);
		$this->load->view('profile/test/sidebar', ['new' => true, 'tests' => $test, 'end' => $maxTime-time(), 'test_id' => $testId]);
		$this->load->view('footer');
	}

	/**
	 * Shows Answers to the Student
	 * @method showAnswers
	 * @param  int      $testId Test ID
	 * @param  int      $utId   Unique Test ID
	 */
	public function showAnswers($testId, $utId)
	{
		$test = $this->Test_Model->test_list($testId, $this->session->userdata('branch'), $this->session->userdata('year'));
		// Test Not yet Ended
		if ($test == null || time() < $test[0]->date_end)
			redirect('test/list');

		$this->load->view('header', array('title' => 'Test'));
		$this->load->view('profile/main');
		$this->load->view('profile/test/exam', ['new' => false, 'questions' => $this->Test_Model->question_list($testId, (intval($test[0]->randomize) == 1)?true:false), 'utId' => $utId]);
		$this->load->view('profile/test/sidebar', ['new' => false, 'tests' => $test, 'test_id' => $testId]);
		$this->load->view('footer');
	}

	/** Processing */

	/**
	 * Retakes a specific test
	 * @method retake
	 * @param  int $test_id Test ID
	 */
	public function retake($test_id)
	{
		$this->takeTest($test_id, false);
	}

	/**
	 * Start the New Test
	 * @method takeNew
	 * @param  int $test_id Test ID
	 */
	public function takeNew($test_id)
	{
		$this->takeTest($test_id, true);
	}

	/**
	 * Saves Answer for given UniqueTestID(Unique per session)
	 * @method saveAnswer
	 * @param  int	$utId	Unique TestID
	 */
	public function saveAnswer($utId)
	{
		// Check Eligibility
		if (!$this->test_helper->checkEligible($utId)) {
			echo 'profile';
			return;
		}
		// Saves the Answer
		$retVal = $this->Test_Model->saveAnswer($utId, intval($this->input->post('question')), intval($this->input->post('choice')), intval($this->input->post('status')));

		if ($retVal == false) { // Cannot Save
			echo 'error';
		}
		echo 'success';
	}

	/**
	 * Gets all the Answers of given UniqueTestId
	 * @method getAnswers
	 * @param  int	$utId	UniqueTestId
	 */
	public function getAnswers($utId)
	{
		if (intval($this->input->post('new')) == 1 && !$this->test_helper->checkEligible($utId)) {
			echo 'profile';
			return;
		}
		// echo json encoded string.
		echo json_encode($this->Test_Model->getAnswers($utId));
	}

	/**
	 * Get Statistics of Questions
	 * Questions Attempted
	 * @method getQuestionStatistics
	 * @param  int	$utId	UniqueTestId
	 */
	public function getQuestionStatistics($utId)
	{
		if (intval($this->input->post('new')) == 1 && !$this->test_helper->checkEligible($utId)) {
			echo 'profile';
			return;
		}
		echo json_encode($this->Test_Model->getQuestionStatistics($utId));
	}

	/**
	 * Ends the Given Test
	 * @method finishTest
	 * @param  int	$utId	UniqueTestId
	 */
	public function finishTest($utId)
	{
		// Check if Test can be ended?
		if (!$this->Test_Model->end_test($utId)) {
			echo 'error';
		} else {
			echo 'success';
		}
		// Set Eligibility to none.
		$this->test_helper->setEligible(0);
	}

	/**
	 * returns the Score of given UniqueTestId
	 * @method getScore
	 * @param  int	$utId	UniqueTestId
	 */
	public function getScore($utId)
	{
		// Get Student Test Details
		$studentDetails = $this->Test_Model->getStudentTest($utId);
		if ($studentDetails != null) {
			$studentDetails[0]->score = intval($studentDetails[0]->score);
			// Generate Score if score is -1
			if ($studentDetails[0]->score == -1) {
				$retArray = $this->Test_Model->generateScore($studentDetails[0]->test_id, $utId, $this->session->userdata('id'));
				if ($retArray[0] == true) {
					echo $retArray[1];
					return;
				}
			} else {
				echo $studentDetails[0]->score;
				return;
			}
		}
		echo 'error';
	}
}
?>