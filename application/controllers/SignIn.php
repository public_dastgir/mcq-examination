<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignIn extends CI_Controller {
	/**
	 * Constructor: Checks if User is validated.
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct();
		$this->check_isvalidated();
	}

	/**
	 * Checks if valid to use this module.
	 * Should be not logged in and not logout url.
	 * @method check_isvalidated
	 */
	private function check_isvalidated()
	{
		if ($this->uri->segment(1) == "logout")
			return;
		if ($this->session->userdata('validated') == '1'){
			redirect('profile');
		}
	}

	/** Views */

	/**
	 * SignIn Page View
	 * @method index
	 */
	public function index()
	{
		$this->load->view('header', array('title' => 'Login'));
		$this->load->view('login/login');
		$this->load->view('footer');
	}

	/**
	 * Registration Page
	 * @method register
	 */
	public function register()
	{
		$this->load->view('header', array('title' => 'Registration'));
		$this->load->view('login/signup');
		$this->load->view('footer');
	}

	/** Processing */

	/**
	 * Processes the login request sent by user.
	 * @method process
	 */
	public function process()
	{
		// Load the model
		$this->load->model('SignIn_Model');
		// Validate the user can login
		$result = $this->SignIn_Model->validate();
		// Now we verify the result
		switch ($result) {
			case 0: // Invalid Username/Password
				$this->session->set_flashdata('error', 'Invalid Username or Password.');
				redirect('');
				break;	
			default: // Not Yet Verified
				$user_hash = password_hash($this->input->post('username'), PASSWORD_BCRYPT, ['cost' => '10']);
				$this->session->set_flashdata('error', 'Account not yet Verified: Resend Verification Email: <a href="'. base_url() .'resend/'. ($result-1) .'/'. base64_encode($user_hash) .'"> Click Here</a>');
				redirect('');
				break;
			case 1: // Verified
				redirect('profile');
				break;
		}
	}

	/**
	 * Processes the Register Request Sent by User
	 * @method register_process
	 */
	public function register_process()
	{
		// Load Various Library/Model/Helper for registration
		$this->load->library(array('form_validation'));
		$this->load->model('SignIn_Model');
		$this->load->helper('security');
		/**
		 * Validation Rules:
		 * Username:	 	should be alphabets, 3-30 characters and unique
		 * First/Last Name:	should be alphabets, 3-30 characters
		 * Email Address:	should be valid
		 * Password:		should match with confirm password
		 * Branch:			should be from list provided(COMP,CIVI,MECH,ELEC,EXTC,BIOT)
		 * Year:			should be between 1-4
		 * UIN:				should be having 7 characters and unique
		 */
		$this->form_validation->set_rules('uname', 'UserName', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirm]');
		$this->form_validation->set_rules('confirm', 'Confirm Password', 'trim|required');
		$this->form_validation->set_rules('year', 'Year', 'trim|required|in_list[1,2,3,4]');
		$this->form_validation->set_rules('branch', 'Branch', 'trim|required|in_list[COMP,CIVI,MECH,ELEC,EXTC,BIOT]');
		$this->form_validation->set_rules('uin', 'UIN', 'trim|required|min_length[7]|xss_clean|is_unique[users.uin]');
		
		
		// Validate the Rules
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			$this->register();
			return;
		}
		// Register and Send Email
		$token = $this->SignIn_Model->register();
		if ($token != null) {
			$this->session->set_flashdata('info', 'Successfully Registered. Please Verify the Email');
			$this->index();
		} else {
			$this->session->set_flashdata('error', 'Could not Register, Try Again Later.');
			$this->register();
		}
	}

	/**
	 * Logout and Destroy Session
	 * @method do_logout
	 */
	public function do_logout()
	{
		$this->session->sess_destroy();
		redirect('sign_in');
	}

	/**
	 * Verify the Email and Account
	 * @method emailVerify
	 * @param  int		$verifyId	User ID
	 * @param  string	$token		Verification Token
	 */
	public function emailVerify($verifyId, $token)
	{
		$this->load->model('SignIn_Model');
		if ($this->SignIn_Model->checkEmail($verifyId, $token)) {
			$this->session->set_flashdata('info', 'Email Verified, You can Login now.');
		} else {
			$this->session->set_flashdata('error', 'Cannot Verify Email Address. (Invalid Email/Token/Link)');
		}
		redirect('sign_in');
	}

	/**
	 * Resend the Verification Email
	 * @method emailVerify
	 * @param  int		$userId	User ID
	 * @param  string	$token	Contains Username in bcrypted form.
	 */
	public function emailResend($userId, $token)
	{
		$this->load->model('SignIn_Model');

		switch ($this->SignIn_Model->resendEmail($userId, $token)) {
			case 0:
			case -1:
				$this->session->set_flashdata('error', 'Cannot send Verification Email. Incorrect Link?');
				break;
			case -2:
				$this->session->set_flashdata('error', 'Cannot send Verification Email too soon. Please wait 24 hours.');
				break;
			case 1:
				$this->session->set_flashdata('info', 'Email Verification Link Sent.');
				break;
		}
		redirect('sign_in');
	}
}
