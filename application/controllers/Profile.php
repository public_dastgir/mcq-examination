<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	/**
	 * Constructor: Checks if User is validated.
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct();
		$this->check_isvalidated();
	}

	/**
	 * Checks for User Validation
	 * @method check_isvalidated
	 */
	private function check_isvalidated()
	{
		if ($this->session->userdata('validated') != true) {
			redirect('sign_in');
		}
	}

	/** Views */

	/**
	 * Profile Home Page(for Students)
	 * @method index
	 */
	public function index()
	{
		$this->load->view('header', array('title' => 'Home Page'));
		$this->load->view('profile/main');
		$this->load->view('profile/content');
		$this->load->view('profile/sidebar');
		$this->load->view('footer');
	}

	/**
	 * Logs Page.
	 * @method view_logs
	 */
	public function view_logs()
	{
		// Sql Logs are loaded and passed to view.
		$this->load->library('Sql_log');

		$this->load->view('header', array('title' => 'View Logs'));
		$this->load->view('profile/main');
		$this->load->view('profile/logs', ['logs' => $this->sql_log->get()]);
		$this->load->view('profile/sidebar');
		$this->load->view('footer');   
	}
}
?>