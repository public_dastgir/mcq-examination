<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	/**
	 * Constructor: Checks if User is admin.
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct();
		$this->check_isvalidated();
	}

	/**
	 * Checks if User can use this controller
	 * @method check_isvalidated
	 */
	private function check_isvalidated()
	{
		if ($this->session->userdata('validated') != true) {
			redirect('sign_in');
		}
		if ($this->session->userdata('role') != 'A') {
			redirect('profile');
		}
	}

	/** Views */

	/**
	 * Home Page for Admin
	 * @method index
	 */
	public function index()
	{
		// Load Requred Model
		$this->load->model('Test_Model');

		// Views to show
		$this->load->view('header', array('title' => 'Admin Panel'));
		$this->load->view('profile/main');
		$this->load->view('admin/content', ['tests' => $this->Test_Model->test_list()]);
		$this->load->view('admin/sidebar');
		$this->load->view('footer');
	}
}
?>