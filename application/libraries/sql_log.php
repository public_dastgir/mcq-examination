<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SQL Logging
 */
class Sql_log {
	private $CI;

	/**
	 * Constructor
	 * Generates CodeIgniter Instance
	 * @method __construct
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	/**
	 * Gets log of type 'x' from user
	 * @method get
	 * @param  string	$logType	LogTypes ('all', 'info', 'error', 'debug')
	 * @param  int		$userId		UserId
	 * @return array				Array Containing SQL Rows object
	 */
	public function get($logType = 'all', $userId = null)
	{
		// Get UserID
		if ($userId == null) {
			$userId = $this->CI->session->userdata('id');
		}

		// Limit Search to specific type.
		if ($logType != 'all') {
			$this->CI->db->where('type', $logType);
		}

		$this->CI->db->where('userid', $userId)
					 ->order_by('id', 'DESC');

		$query = $this->CI->db->get('logs');

		if ($query->result_id->num_rows > 0) {
			$rw = $query->result();
			return $rw;
		}
		
		return NULL;
	}

	/**
	 * Inserts Log to Database
	 * @method insert
	 * @param  int		$logType	LogTypes ('info', 'error', 'debug')
	 * @param  string	$message	Message to Insert
	 * @param  int		$userId		UserID
	 * @param  string   $table      Table of Logs
	 * @return boolean				True, if succeed, else false.
	 */
	private function insert($logType, $message, $userId = null, $table = 'logs')
	{
		// Gets UserId from Session
		if ($userId == null) {
			$userId = $this->CI->session->userdata('id');
		}
		// Prepares Data to be inserted.
		$data = array(
			'type' => $logType,
			'userid' => $userId,
			'message' => $message,
			'ip' => $this->CI->input->ip_address(),
		);
			
		// Insert
		if ($this->CI->db->insert($table, $data)) {
			log_message('info', 'Logged Success');
			return true;
		}
		log_message('error', 'Logging Failed');
		return false;
	}

	/**
	 * Inserts Log with 'info' logType
	 * @method info
	 * @param  string	$message	Message to Insert
	 * @param  int		$userId		UserId
	 * @param  string   $table      Table to insert
	 */
	public function info($message, $userId = null, $table = 'logs')
	{
		$this->insert('info', $message, $userId, $table);
	}

	/**
	 * Inserts Log with 'error' logType
	 * @method error
	 * @param  string	$message	Message to Insert
	 * @param  int		$userId		UserId
	 * @param  string   $table      Table to insert
	 */
	public function error($message, $userId = null, $table = 'logs')
	{
		$this->insert('error', $message, $userId, $table);
	}

	/**
	 * Inserts Log with 'debug' logType
	 * @method debug
	 * @param  string	$message	Message to Insert
	 * @param  int		$userId		UserId
	 * @param  string   $table      Table to insert
	 */
	public function debug($message, $userId = null, $table = 'logs')
	{
		$this->insert('debug', $message, $userId, $table);
	}

	/**
	 * Inserts Log with 'warning' logType
	 * @method warning
	 * @param  string	$message	Message to Insert
	 * @param  int		$userId		UserId
	 * @param  string   $table      Table to insert
	 */
	public function warning($message, $userId = null, $table = 'logs')
	{
		$this->insert('warning', $message, $userId, $table);
	}
}
?>