<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Test_Helper
 */
class Test_Helper {
	private $CI;

	/**
	 * Constructor
	 * Generates CodeIgniter Instance
	 * @method __construct
	 */
	public function __construct() {
		$this->CI =& get_instance();
	}

	/**
	 * Sets MaxTime of Given Test
	 * @method setMaxTime
	 * @param	int	$maxTime MaxTime
	 */
	public function setMaxTime($maxTime)
	{
		$this->CI->session->set_userdata('test_maxTime', $maxTime);
	}

	/**
	 * Checks if Max time is within the limit.
	 * @method checkMaxTime
	 * @return boolean		false, if time is not left, else true
	 */
	public function checkMaxTime()
	{
		if (time() >= $this->CI->session->userdata('test_maxTime'))
			return false;
		return true;
	}

	/**
	 * Sets Eligible Variable to UniqueTestId
	 * @method setEligible
	 * @param	int	$uniTestId UniqueTestId 
	 */
	public function setEligible($uniTestId)
	{
		$this->CI->session->set_userdata('test_eligible', $uniTestId);
		return true;
	}

	/**
	 * Checks if User Test and variable UniqueTestId Matches
	 * @method checkEligible
	 * @param  int	$testId	UniqueTestId
	 * @return bool			false, if not same, else returns checkMaxTime()
	 */
	public function checkEligible($testId)
	{
		if ($this->CI->session->userdata('test_eligible') != $testId)
			return false;
		return $this->checkMaxTime();
	}
}
?>