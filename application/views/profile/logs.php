<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="table-responsive">
				<div class="body">
					<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th>Sr#</th>
								<th>Message</th>
								<th>Type</th>
								<th>Date</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sr#</th>
								<th>Message</th>
								<th>Type</th>
								<th>Date</th>
							</tr>
						</tfoot>
						<tbody>
							<?php
							for ($i = 0; $i < count($logs); $i++) {
								echo '<tr>';
									echo '<td>'. ($i+1) .'</td>';
									echo '<td>'. $logs[$i]->message .'</td>';
									echo '<td>'. $logs[$i]->type .'</td>';
									echo '<td>'. $logs[$i]->log_created .'</td>';
								echo '</tr>';
							}
							?>
					  	</tbody>
				  	</table>
				</div>
			</div>
		</div>
	</div>
</div>
</section>