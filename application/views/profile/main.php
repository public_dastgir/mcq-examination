<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if ($this->uri->segment(1) == "admin") { ?>
<body class="theme-deep-purple">
<?php } else { ?>
<body class="theme-blue">
<?php } ?>
	<!-- navbar -->
	<div>
		<nav class="navbar">
			<div class="container-fluid">
				<div class="navbar-header">
					<a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
					<a href="javascript:void(0);" class="bars" style="display: none;"></a>
					<a class="navbar-brand" href="<?php echo base_url();?>profile">RCOE</a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#"><?php echo $this->session->userdata('name'); ?></a></li>
						<li><a href="<?php echo base_url();?>logout">Logout</a></li>
						<?php if ($this->session->userdata('role') == 'A') { ?>
							<?php if ($this->uri->segment(1) == "admin") { ?>
								<li><a href="<?php echo base_url();?>profile">Student View</a></li>
							<?php } else { ?>
								<li><a href="<?php echo base_url();?>admin">Admin Panel</a></li>
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
			</div>
		</nav>
	</div>
?>