<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="sidebar">
	<br/>
	<div class="menu">
		<ul class="list">
			<li class="active">
				<a href="<?php echo base_url(); ?>profile">Home Page</a>
				<h6 align="center">Test Management</h6>
				<a href="<?php echo base_url(); ?>test/list">List Tests</a>
				<a href="<?php echo base_url(); ?>test/listScores">View Scores</a>
				<hr>
				<h6 align="center">Profile Management</h6>
				<a href="<?php echo base_url(); ?>profile/logs">View Logs</a>
				<hr>
			</li>
		</ul>
	</div>
</section>