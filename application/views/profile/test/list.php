<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
	<div class="col-xs-12">
		<div class="card">
			<div class="header">
			</div>
			<div class="body">
				<div class="table-responsive">
					<!-- table table-bordered table-striped table-hover js-basic-example dataTable display nowrap -->
					<table class="js-basic-example display nowrap table table-bordered table-striped table-hover dataTable" style="width: 100%" data-order='[[ 2, "desc" ]]'>
						<thead>
							<tr>
								<th>Test#</th>
								<th>Name</th>
								<?php if ($type == 'test') { ?>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Added By</th>
								<th>Q#</th>
								<th>Minutes</th>
								<th>Actions</th>
								<?php } else { ?>
								<th>Test Date</th>
								<th>Score</th>
								<th>Percentage</th>
								<?php } ?>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Test#</th>
								<th>Name</th>
								<?php if ($type == 'test') { ?>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Added By</th>
								<th>Q#</th>
								<th>Minutes</th>
								<th>Actions</th>
								<?php } else { ?>
								<th>Test Date</th>
								<th>Score</th>
								<th>Percentage</th>
								<?php } ?>
							</tr>
						</tfoot>
						<tbody>
							<?php
							// System Status
							define('STATUS_NOT_STARTED', 0x01);
							define('STATUS_STARTED',	 0x02);
							define('STATUS_ENDED',	   0x04);

							define('STATUS_T_NOT_TAKEN', 0x10);
							define('STATUS_T_ONGOING',   0x20);
							define('STATUS_T_TAKEN',	 0x40);
							$sr_id = 1;
							for ($i = 0; $i < count($tests); $i++) {
								if (!$this->Test_Model->eligible($tests[$i]->id, false)) {
									continue;
								}
								$testDetails = $this->Test_Model->check_all_tests($tests[$i]->id);
								for ($j = 0; ($j < count($testDetails) && $type == 'score') || ($j == 0 && $type == 'test'); $j++) {
									$status = 0; // < 0 : not yet started, 0 : started, 1 : test started by user, 2: Test ended by user, 4 : ended
									if ($type == 'test') {
										$timeRet = -1;
										if ($testDetails != null) {
											$timeRet = $this->Test_Model->timeRemaining($testDetails[$j]->id);
										}
										// Check System Status
										if (time() >= $tests[$i]->date_end) {
											$bg = 'bg-red';
											$status |= STATUS_ENDED;
										} else if (time() < $tests[$i]->date_start) {
											$bg = 'bg-yellow';
											$status |= STATUS_NOT_STARTED;
										} else {
											$bg = 'bg-green';
											$status |= STATUS_STARTED;
										}
										// Individual Status
										if ($timeRet > 0) {
											$bg = 'bg-blue';
											$status |= STATUS_T_ONGOING;
										} else if ($timeRet == 0) {
											$bg = 'bg-red';
											$status |= STATUS_T_TAKEN;
										} else {
											$bg = 'bg-green';
											$status |= STATUS_T_NOT_TAKEN;
										}
										if ($status&(STATUS_T_NOT_TAKEN|STATUS_ENDED) == (STATUS_T_NOT_TAKEN|STATUS_ENDED)) {
											continue;	// Not Taken + Ended, so don't show log
										}
										// Background Color
										if ($status&STATUS_STARTED) {
											if ($status&STATUS_T_NOT_TAKEN) {
												$bg = 'bg-green';
											} else if ($status&STATUS_T_ONGOING) {
												$bg = 'bg-cyan';
											} else if ($status&STATUS_T_TAKEN) {
												$bg = 'bg-orange';
											}
										} else if ($status&STATUS_NOT_STARTED) {
											$bg = 'bg-blue';
										} else if ($status&STATUS_ENDED) {
											$bg = 'bg-red';
										}
									} else {
										if (($testDetails[$j]->score/$tests[$i]->questions)*100 < 40) {
											$bg = 'bg-red';
											$progress_bg = 'danger';
										} else {
											$bg = 'bg-green';
											$progress_bg = 'success';
										}
									}
									echo '<tr class="'. $bg .'" id="t'. $tests[$i]->id .'">';
										echo '<td id="ti'. $tests[$i]->id .'">'. ($sr_id++) .'</td>';
										echo '<td>'. $tests[$i]->name .'</td>';
									if ($type == 'test') {
										echo '<td>'. date('Y-m-d H:i:s', $tests[$i]->date_start) .'</td>';
										echo '<td>'. date('Y-m-d H:i:s', $tests[$i]->date_end) .'</td>';
										echo '<td>'. $tests[$i]->fname .' '. $tests[$i]->lname .' </td>';
										echo '<td>'. $tests[$i]->questions .'</td>';
										echo '<td>'. $tests[$i]->time .'</td>';
										echo '<td>';
										if ($status&STATUS_NOT_STARTED) {	// Test Not Yet Available
											echo '<a class="btn btn-primary waves-effect">No Actions</a>';
										} else if ($status&STATUS_STARTED) { // Test Ended by Student
											if ($status&STATUS_T_TAKEN) {
											?>
												<div class="btn-group">
													<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														Actions <span class="caret"></span>
													</button>
													<ul class="dropdown-menu">
														<li>
															<a onclick="return alertUser(this, '<?php echo base_url();?>test/retake/<?php echo $tests[$i]->id; ?>')">Retake</a>
														</li>
														<li role="separator" class="divider"></li>
														<li><a href="javascript:void(0);" onclick="getScore(<?php echo $testDetails[$j]->id .','. $tests[$i]->questions; ?>, 1);">View Score</a></li>
													</ul>
												</div>
											<?php
											} else if ($status&STATUS_T_NOT_TAKEN) {
												echo '<a class="btn btn-primary waves-effect" onclick="return alertUser(this, \''. base_url() .'test/take/'. $tests[$i]->id .'\');">Take Test</a>';
											} else if ($status&STATUS_T_ONGOING) {
												echo '<a class="btn btn-primary waves-effect" onclick="return alertUser(this, \''. base_url() .'test/take/'. $tests[$i]->id .'\');">Resume Test</a>';
											}
									 	} else if ($status&STATUS_ENDED) { // Test Ended by System
									 		if ($status&STATUS_T_NOT_TAKEN) {
									 			echo '<a class="btn btn-primary waves-effect">No Actions</a>';
									 		} else {
									 		?>
									 			<div class="btn-group">
													<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														Actions <span class="caret"></span>
													</button>
													<ul class="dropdown-menu">
														<li><a href="<?php echo base_url();?>test/answers/<?php echo $tests[$i]->id; ?>/<?php echo $testDetails[$j]->id; ?>">View Answers</a></li>
														<li role="separator" class="divider"></li>
														<li><a href="javascript:void(0);" onclick="getScore(<?php echo $testDetails[$j]->id .','. $tests[$i]->questions; ?>, 1);">View Score</a></li>
													</ul>
												</div>
									 		<?php
											}
									 	}
										echo '</td>';
									} else {
										echo '<td>'. date('Y-m-d H:i:s', $testDetails[$j]->time_start) .'</td>';
										echo '<td>'. $testDetails[$j]->score .' / '. $tests[$i]->questions .'</td>';
										echo '<td>';
										$percent = floor(($testDetails[$j]->score/$tests[$i]->questions)*100);
										if ($testDetails[$j]->status == 0) {
											echo '<a class="btn btn-primary waves-effect" onclick="getScore('. $testDetails[$j]->id .','. $tests[$i]->questions .', 0, this);">View Score</a>';
										} else {
								?>
											<div class="progress">
												<div class="progress-bar progress-bar-<?php echo $progress_bg; ?> progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent; ?>%">
													<span class="sr-only" style="position: relative; color: black;"><?php echo $percent.'%'; ?></span>
												</div>
											</div>
							   	<?php
							   			}
										echo '</td>';
									}
									echo '</tr>';
								}
							}
							?>
					  	</tbody>
				  	</table>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	function alertUser(elem, url) {
		retVal = false;
		swal({
	        title: "Start the Test?",
	        text: "Once started, You cannot pause the test\nTimer will keep running\nPress \'Start!\' to Confirm, Please \'Cancel\' to start test later",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Start!",
	        closeOnConfirm: false,
	        allowEscapeKey: false
	    }, function() {
	    	//toggleFullscreen(true);
	    	swal.close();
	    	window.location.href = url;
	    });
	    //return false;
	    /*
		var retVal = confirm('Once started, You cannot pause the test\nTimer will keep running\nPress \'OK\' to Confirm, Please \'Cancel\' to start test later');
		if (retVal == true)
			toggleFullscreen();
		*/
		return retVal;
	}

	function setPercent(ele, percent) {
		html = '<div class="progress">'+
				'<div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="'+ percent +'" aria-valuemin="0" aria-valuemax="100" style="width: '+ percent +'%">'+
				'<span class="sr-only" style="position: relative; color: black;">'+ percent +'%</span>'+
			'</div>'+
		'</div>';
		ele.outerHTML = html;
	}

	function getScore(utId, totalQuestions, status, ele) {
		var url = '<?php echo base_url();?>test/score/' + utId;
		$.ajax({
			type: "POST",
			url: url,
			data: '',
			success: function(data) {
				switch(data) {
					default:
						data = parseInt(data);
						percent = (data/totalQuestions*100);
						color = 'danger';
						if (percent > 75) {
							color = 'success';
						} else if (percent > 35) {
							color = 'warning';
						}
						notify('Result', 'Results are out!<br>Marks: '+ data +'/'+ totalQuestions +'<br>Percentage: '+ percent +'%', color);
						if (status == 0 && ele != undefined) {
							setPercent(ele, parseInt(percent));
						}
						break;
					case 'error':
						notify('Result', 'Server Error: Please Try Again Later.', 'error');
						break;
				}
			},
			error: function(data) {
				notify('Result', 'Server Error: Please Try Again Later.', 'error');
			},
			timeout: 10000,
		});
	}

</script>
</section>