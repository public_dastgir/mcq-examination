<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
	<div class="col-xs-12">
		<div class="card">
			<div class="header bg-green">
				Question#<strong id="question_number"></strong>
			</div>
			<div class="body">
				<div class="card">
					<div class="header" id="question_text">
						
					</div>
				</div>
				<div class="card">
					<div class="body">
						<div class="row">
						<?php
							for ($i = 1; $i <= 6; $i++) {
						?>
							<div id="answer_div<?php echo $i; ?>" class="col-sm-6" hidden>
								<input <?php if (!$new) { echo 'disabled'; } ?> type="checkbox" id="q<?php echo $i; ?>" choice="<?php echo $i; ?>" name="cb<?php echo $i; ?>" class="chk-col-green" />
								<label for="q<?php echo $i; ?>" id="answer<?php echo $i; ?>">Answer 1</label>
							</div>
						<?php
							}
						?>
						</div>
						<div class="row"">
							<button type="button" class="col-sm-1 col-sm-offset-1 btn btn-info btn-circle waves-effect waves-circle waves-float" id="button_prev" onclick="prevQuestion();">
								<i class="material-icons">navigate_before</i>
							</button>
							<button type="button" class="col-sm-1 col-sm-offset-8 btn btn-info btn-circle waves-effect waves-circle waves-float" id="button_next" onclick="nextQuestion();">
								<i class="material-icons">navigate_next</i>
							</button>
						   </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	var current_question = 1;
	var question_text = document.getElementById('question_text');
	var answer_text = [];
	var btn_next = document.getElementById('button_next');
	var btn_prev = document.getElementById('button_prev');
	var status_text;
	var status_preloader;
	var uni_test_id = <?php echo $utId; ?>;
	var answers_marked = [];
	var ready = false;
	var shuffle_index = [];

	function shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}

	function alertUser() {
<?php if ($new) { ?>
		var retVal = confirm('Once started, You cannot pause the test\nTimer will keep running\nPress \'OK\' to Confirm, Please \'Cancel\' to start test later');
		return retVal;
<?php } else { ?>
		return true;
<?php } ?>
	}
	// Questions List
	var question_list = [
<?php
		for ($i = 0; $i < count($questions); $i++) {
			echo "[";
				// Question
				echo $this->db->escape($questions[$i]->question) .",";
				// Options
				echo "[". $this->db->escape($questions[$i]->a) .", ". $this->db->escape($questions[$i]->b) .", ". $this->db->escape($questions[$i]->c) .", ". $this->db->escape($questions[$i]->d) .", ". $this->db->escape($questions[$i]->e) .", ". $this->db->escape($questions[$i]->f) .", ],";
				echo ($i+1) .",";
				echo $questions[$i]->id .",";
			if (!$new) {
				echo $questions[$i]->correct .",";
			}
			echo "],";
		}
?>
	];


	$(document).ready(function() {
		question_list = shuffle(question_list);
		prepareQuestionIndex();

		status_text = document.getElementById('status_text');;
		status_preloader = document.getElementById('status_preloader');
		for (var i = 0; i < 6; i++) {
			answer_text[i] = document.getElementById('answer'+ (i+1));
	<?php if ($new) { ?>
			document.getElementById('q'+ (i+1)).addEventListener('change', function(event) {
				var answerChoice = parseInt(event.target.getAttribute('choice'));
				var status = event.target.checked?1:0
				saveAnswer(uni_test_id, answerChoice, status);
			});
	<?php } ?>
		}
		emptyAnswers();
		$('#button_prev').attr('disabled', 'true');
		getAnswers(false);
		showQuestion();
	});

	function showQuestion() {
		question_text.innerHTML = question_list[current_question-1][0];
		var i = 0;
		for (i = 0; i < question_list[current_question-1][1].length; i++) {
			document.getElementById('q'+ (i+1)).checked = false;
			var answer = question_list[current_question-1][1][i];
			if (answer == '') {
				document.getElementById('answer_div'+ (i+1)).hidden = true;
			} else {
				document.getElementById('answer_div'+ (i+1)).hidden = false;
				answer_text[i].innerHTML = answer;
			}
			markAnswers()
		}
		document.getElementById('question_number').innerHTML = current_question;
	}

	function nextQuestion() {
		toQuestion(current_question+1);
	}

	function prevQuestion() {
		toQuestion(current_question-1);
	}

	function toQuestion(qNo) {
		// Same Question
		if (qNo == current_question) {
			return false;
		}
		// Beyond Last Question
		if (qNo > question_list.length) {
			return false;
		} else if (qNo < 1) {	// < First Question
			return false;
		}

		// Button Disable
		if (qNo == question_list.length) {
			btn_next.disabled = true;
		} else if (qNo == 1) {
			btn_prev.disabled = true;
		}
		// Button Enable
		if (qNo != 1 && btn_prev.disabled == true) {
			btn_prev.disabled = false;
		} else if (qNo != question_list.length && btn_next.disabled == true) {
			btn_next.disabled = false;
		}
		if (!checkFullscreen() && !testFinished) {
			toggleFullscreen(true);
			lose_focus += 1;
			notify('Cheating Attempt!', "Changing to Windowed mode counts as cheating");
		}
		// Change Current Question
		current_question = qNo;
		showQuestion();
		return true;
	}

<?php if ($new) { ?>
	function saveAnswer(utest_id, question_number, status) {
		var url = '<?php echo base_url();?>test/saveAnswer/' + utest_id;
		var other_status = ((status == 1)?false:true);
		if (!ready) {
			document.getElementById('q'+ question_number).checked = other_status;
			return;
		}
		var question_attempting = current_question;
		changeStatus('Saving Answer...', false);
		$.ajax({
			type: "POST",
			url: url,
			data: {
				'question': question_list[question_attempting-1][3],
				'choice': question_number,
				'status': status,
			},
			success: function(data) {
				switch(data) {
					case 'success':
						if (status == 1) {
							answers_marked[question_attempting].push(question_number);
						} else if (status == 0) {
							var index = answers_marked[question_attempting].indexOf(question_number);
							if (index > -1) {
								answers_marked[question_attempting].splice(index, 1);
							}
						}
						if (answers_marked[question_attempting].length > 0) {
							questionAttempted(question_attempting);
						} else {
							questionRemoved(question_attempting);
						}
						changeStatus('Answer Saved', true);
						break;
					default:
					case 'error':
						document.getElementById('q'+ question_number).checked = other_status;
						changeStatus('Answer Cannot be Saved.  Sync Choice button has been enabled...', true);
						sync_button(false);
						break;
					case 'profile':
						document.getElementById('q'+ question_number).checked = other_status;
						changeStatus('Error. Redirecting to Profile', false);
						notify('Error', 'Something bad happened! Redirecting to Profile', 'danger');
						setTimeout(function() {
							location.href = '<?php echo base_url();?>profile';
						}, 5000);
						break;
				}
			},
			error: function() {
				document.getElementById('q'+ question_number).checked = other_status;
				changeStatus('Answer Cannot be Saved.  Sync Choice button has been enabled.', true);
				sync_button(false);
			},
			timeout: 10000,
		});
		return false;
	}

<?php } ?>

	function getAnswers(refreshAnswers) {
		changeStatus('Checking Questions and Answers', false);
		ready = false;
		emptyAnswers();
		var url = '<?php echo base_url();?>test/getAnswers/' + uni_test_id;
		$.ajax({
			type: "POST",
			url: url,
			data: {
				'new': <?php echo ($new == 1)?1:0; ?>,
			},
			success: function(data) {
				switch(data) {
					default:
						mergeAnswers($.parseJSON(data));
						markAnswers();
						changeStatus('Ready', true);
						ready = true;
						if (refreshAnswers) {
							showQuestion();
						}
						break;
					case 'profile':
						changeStatus('Error. Redirecting to Profile', false);
						notify('Error', 'Something bad happened! Redirecting to Profile', 'danger');
						setTimeout(function() {
							location.href = '<?php echo base_url();?>profile';
						}, 5000);
						break;
				}
			},
			error: function(data) {
				changeStatus('Server Error: Please Try Again Later.', false);
			},
			timeout: 10000,
		});
		return false;
	}

	function emptyAnswers() {
		for (var i = 1; i <= <?php echo count($questions); ?>; i++) {
			answers_marked[i] = [];
		}
	}

	function getCurrentQuestion() {
		return shuffle_index[current_question];
	}

	function getQuestionIndex(cQuestion) {
		for (var i = 0; i < <?php echo count($questions); ?>; i++) {
			if (question_list[i][3] == cQuestion)
				return i;
		}
	}

	function prepareQuestionIndex() {
		shuffle_index[0] = 0;
		for (var i = 1; i <= <?php echo count($questions); ?>; i++) {
			for (var j = 1; j <= <?php echo count($questions); ?>; j++) {
				if (question_list[i-1][2] == j)
					shuffle_index[j] = i;
			}
		}
	}

	function markAnswers() {
<?php if (!$new) { ?>
		correct_answers = [];
		var correct = question_list[current_question-1][4];
		for (i = 1; i <= 6; i++) {
			if ((correct&(1<<(i-1))) > 0) {
				correct_answers.push(i);
			}
		}
<?php } ?>
		for (i = 0; i < answers_marked[current_question].length; i++) {
			if (answers_marked[current_question][i] < question_list[current_question-1][1].length) {
				var q = document.getElementById('q'+ answers_marked[current_question][i]);
				q.checked = true;
				<?php if (!$new) { ?>
					// Chose + Correct
					if (correct_answers.indexOf(answers_marked[current_question][i]) > -1) {
						q.className = "chk-col-green";
					} else {
						q.className = "chk-col-red";
					}
				<?php } ?>
			}
		}
<?php if (!$new) { ?>
		for (i = 0; i < correct_answers.length; i++) {
			var q = document.getElementById('q'+ correct_answers[i]);
			if (!q.checked) {
				q.checked = true;
				q.className = 'chk-col-green';
			}
		}
<?php } ?>
	}

	function mergeAnswers(answer_temp) {
		for (var k in answer_temp){
			if (answer_temp.hasOwnProperty(k)) {
				answers_marked[getQuestionIndex(k)+1] = answer_temp[k];
			}
		}
	}

	function changeStatus(text, hidePreloader) {
		status_preloader.hidden = hidePreloader;
		status_text.innerHTML = text;
	}

</script>
</section>