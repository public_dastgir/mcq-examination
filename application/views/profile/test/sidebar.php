<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="sidebar">
	<br/>
	<div class="menu">
		<ul class="list">
			<li class="active">
				<a href="<?php echo base_url(); ?>profile" onclick="return alertHomePage()">Back to Home Page</a>
			<?php if ($new) { ?>
				<hr>
				<h6 align="center">Total Time: <?php echo $tests[0]->time; ?> Minutes</h6>
				<h6 align="center">Time Left: <span id="time"></span> Minutes Left</h6>
			<?php } ?>
				<hr>
				<h6 align="center">Total Attempted: <i id="qAttempted">0</i> / <?php echo $tests[0]->questions; ?> </h6>
				<hr>
				<h6 align="center">Question Navigation</h6>
			<?php
				$tagStarted = false;
				for ($i = 0; $i < $tests[0]->questions; $i++) {
					if ($i%7 == 0) { echo '<div class="clickable_span col-xs-12" align="center">'; $tagStarted = true;}
					echo '<span id="sq'. ($i+1) .'" name="'. ($i+1) .'" class="label bg-grey">'. str_pad($i + 1, 2 , '0', STR_PAD_LEFT) .'</span>';
					if (($i+1)%7 == 0) { echo '</div>'; $tagStarted = false;}
				}
				if ($tagStarted) {
					echo '</div>';
				}
			?>
				<br/><hr>
			<?php if ($new) { ?>
				<h6 align="center">Tools</h6>
				<div align="center">
					<button type="button" class="btn waves-effect btn-success" onclick="finishTestButton();">Submit Test</button>
					<button id="sync_choice" type="button" class="btn waves-effect btn-success" onclick="sync_button(true); getAnswers(true);" disabled>Sync Choices</button>
				</div>
				<hr>
			<?php } ?>
				<h6 align="center">Status</h6>
				<div align="center">
					<div class="preloader pl-size-xs">
						<div class="spinner-layer pl-green" id="status_preloader" hidden>
							<div class="circle-clipper left">
								<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
								<div class="circle"></div>
							</div>
						</div>
					</div>
					<strong id="status_text"></strong>
				</div>
			</li>
		</ul>
	</div>
<style type="text/css">
	div.clickable_span span { cursor: pointer; cursor: hand; margin: 5px; }
	div.clickable_span { margin-top: 8px; margin-bottom: 8px; }
</style>
<script type="text/javascript">
	function alertHomePage() {
	<?php if ($new) { ?>
		var retVal = confirm('You cannot pause the test, Timer will keep running\nPress \'OK\' to go back to Home Page, Please \'Cancel\' to continue');
		return retVal;
	<?php } else { ?>
		return true;
	<?php } ?>
	}

	var questions_attempted_total = 0;
	var testFinished = false;

<?php if ($new) { ?>
	var completeShown = false;

	function startTimer(duration, display) {
		var start = Date.now(),
			diff,
			minutes,
			seconds;
		function timer() {
			// get the number of seconds that have elapsed since 
			// startTimer() was called
			diff = duration - (((Date.now() - start) / 1000) | 0);

			// does the same job as parseInt truncates the float
			minutes = (diff / 60) | 0;
			seconds = (diff % 60) | 0;

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;

			display.textContent = minutes + ":" + seconds; 
			if (seconds == 0 && minutes == 0) {
				clearInterval(timerId);
				finishTest();
			}

			if (diff <= 0) {
				start = Date.now() + 1000;
			}

			/*
			if (!checkFullscreen())
				testAlert();
			*/
		};
		// we don't want to wait a full second before the timer starts
		timer();
		var timerId = setInterval(timer, 1000);
	}

	function paperOver() {
		if (!completeShown) {
			notify('Information', 'You can press \'Submit Test\' to finish the paper instantly.<br>Else you can review the questions attempted by you.');
			completeShown = true;
		}
	}

	function questionRemoved(qN) {
		var a = document.getElementById('sq'+ qN);
		if (!a.classList.contains('bg-grey')) {
			a.classList.add('bg-grey');
			if (a.classList.contains('bg-green')) {
				a.classList.remove('bg-green');
			}
			updateQuestionCounter(-1);
		}
	}

	function finishTestButton() {
		var retVal = confirm('Are you sure you want to finish the test?\n You have some time left.');
		if (retVal == true) {
			finishTest();
		}
	}

	function finishTest() {
		testFinished = true;
		var url = '<?php echo base_url();?>test/finish/' + uni_test_id;
		$.ajax({
			type: "POST",
			url: url,
			data: {test_id: <?php echo $test_id; ?>},
			success: function(data) {
				switch(data) {
					default:
						notify('Information', 'Test Finished', 'success');
						location.href = '<?php echo base_url();?>profile';
						break;
					case 'error':
						notify('Test Finished', 'If you do not see the scores, Follow These Steps:<br>1) Go to View Scores<br>2) Click Generate Score for respective test', 'success');
						setTimeout(function() {
							location.href = '<?php echo base_url();?>profile';
						}, 5000);
						break;
				}
			},
			error: function(data) {
				notify('Test Finished', 'If you do not see the scores, Follow These Steps:<br>1) Go to View Scores<br>2) Click Generate Score for respective test', 'danger');
				setTimeout(function() {
					location.href = '<?php echo base_url();?>profile';
				}, 5000);
			},
			timeout: 10000,
		});
		return false;
	}

	function sync_button(disableSync) {
		document.getElementById('sync_choice').disabled = disableSync;
	}

<?php } ?>

	$(document).ready(function() {
		swal({
	        title: "Fullscreen Mode!",
	        text: "It is advised to give the test in fullscreen mode, click 'OK' to enter fullscreen mode.",
	        showCancelButton: false,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "OK",
	        closeOnConfirm: false,
	        allowEscapeKey: false
	    }, function() {
	    	toggleFullscreen(true);
	    	swal.close();
	    	//window.location.href = url;
	    });
		// Toggle Full Screen mode
	<?php if ($new) { ?>
		var fiveMinutes = <?php echo $end; ?>,
			display = document.querySelector('#time');
		startTimer(fiveMinutes, display);
	<?php } ?>
		// On Click Events to question numbers
		for (var i = 1; i <= <?php echo $tests[0]->questions; ?>; i++) {
			var temp = document.getElementById('sq'+ i);
			temp.addEventListener('click', function(event) {
				toQuestion(parseInt(event.target.getAttribute('name')));
			}, false);
		}
		questionStatistics();
	});

	function questionAttempted(qN) {
		var a = document.getElementById('sq'+ qN);
		if (!a.classList.contains('bg-green')) {
			a.classList.add('bg-green');
			if (a.classList.contains('bg-grey')) {
			   a.classList.remove('bg-grey');
			}
			updateQuestionCounter(1);
		}
	<?php if ($new) { ?>
		if (questions_attempted_total == <?php echo $tests[0]->questions; ?>) {
			paperOver();
		}
	<?php } ?>
	}

	function updateQuestionCounter(delta) {
		questions_attempted_total += delta;
		var a = document.getElementById('qAttempted');
		a.innerHTML = questions_attempted_total+'';
	}

	function questionStatistics() {
		var url = '<?php echo base_url();?>test/getQuestionStatistics/' + uni_test_id;
		$.ajax({
			type: "POST",
			url: url,
			data: {
				'new': <?php echo ($new == 1)?1:0; ?>,
			},
			success: function(data) {
				switch(data) {
					default:
						obj = $.parseJSON(data);
						for (var k in obj) {
							questionAttempted(getQuestionIndex(obj[k])+1);
						}
						for (var i = 1; i <= <?php echo $tests[0]->questions; ?>; i++) {
							if (document.getElementById('sq'+ i).classList.contains('bg-grey')) {
								// Change Current Question
								toQuestion(i);
								break;
							}
						}
						break;
					case 'profile':
						notify('Error', 'Something bad happened!<br>Test may be completed<br>Redirecting to Profile', 'danger');
						setTimeout(function() {
							location.href = '<?php echo base_url();?>profile';
						}, 5000);
						break;
				}
			},
			error: function(data) {
				changeStatus('Server Error: Please Try Again Later.', false);
			},
			timeout: 10000,
		});
		return false;
	}

	var lose_focus = 0, is_blur = 0;

	/**
	 * Alert if cheating
	 * @todo Only clientside validation for now.
	 * @method testAlert
	 */
	function testAlert() {
		lose_focus += 1;
		if (lose_focus > 3) {	// End Test
			finishTest();
		}
	}


	$(window).focus(function() {
		if (is_blur > 0 && !testFinished) {
			notify('Cheating Attempt!', "Changing tabs count as cheating");
			testAlert();
			is_blur = 0;
		}
	});

	$(window).blur(function() {
		if (!testFinished) {
			notify('Cheating Attempt!', "");
			is_blur += 1;
		}
	});
</script>
</section>