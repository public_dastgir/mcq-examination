<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="table-responsive">
				<div class="body">
					<table class="table table-bordered table-striped table-hover js-basic-example dataTable"  data-order='[[ 2, "desc" ]]'>
						<thead>
							<tr>
								<th>Test#</th>
								<th>Name</th>
								<th>Date</th>
								<th>Added By</th>
								<th>Q#</th>
								<th>For</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Test#</th>
								<th>Name</th>
								<th>Date</th>
								<th>Added By</th>
								<th>Q#</th>
								<th>For</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</tfoot>
						<tbody>
							<?php
							$time = time();
							for ($i = 0; $i < count($tests); $i++) {
								echo '<tr id="t'. $tests[$i]->id .'">';
									echo '<td id="ti'. $tests[$i]->id .'">'. $tests[$i]->id .'</td>';
									echo '<td>'. $tests[$i]->name .'</td>';
									if ($time < $tests[$i]->date_start) {
										$status = 'Not Started';
										$date_string = date('Y-m-d H:i:s', $tests[$i]->date_start);
									} else if ($time <= $tests[$i]->date_end) {
										$status = 'Started';
										$date_string = date('Y-m-d H:i:s', $tests[$i]->date_end);
									} else {
										$status = 'Ended';
										$date_string = date('Y-m-d H:i:s', $tests[$i]->date_end);
									}
									echo '<td>'. $date_string .'</td>';
									echo '<td>'. $tests[$i]->fname .' '. $tests[$i]->lname .'</td>';
									echo '<td>'. $tests[$i]->questions .'</td>';
									switch($tests[$i]->year) {
										case 1: $year = 'F.E'; break;
										case 2: $year = 'S.E'; break;
										case 3: $year = 'T.E'; break;
										case 4: $year = 'B.E'; break;
									}
									echo '<td>'. $year .' '. $tests[$i]->branch .'</td>';
									echo '<td>'. $status .'</td>';
									echo '<td>';
							?>
										<div class="btn-group">
											<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Actions <span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="<?php echo base_url();?>admin/test/edit/<?php echo $tests[$i]->id; ?>">Edit</a></li>
												<li><a href="javascript:void(0);" onclick="return deleteCall(<?php echo $tests[$i]->id; ?>);">Delete</a></li>	<!-- @todo Alert Box -->
												<li role="separator" class="divider"></li>
												<li><a href="<?php echo base_url();?>admin/test/stats/<?php echo $tests[$i]->id; ?>">View Statistics</a></li>
											</ul>
										</div>
							<?php
									echo '</td>';
								echo '</tr>';
							}
							?>
					  	</tbody>
				  	</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var delete_test_link = '<?php echo base_url();?>admin/test/remove/';
function deleteCall(test_id) {
	var txt = "Press 'OK' to Confirm Deletion of Test #"+ test_id +"\n" +
			  "Once deleted, Test and it's Questions cannot be recovered.\n" +
			  "Press 'Cancel' to cancel the delete process.";
	if (confirm(txt) == true) {
		deleteTest(test_id);
	} else {
		return false;
	}
	return true;
}
function deleteTest(test_id) {
	var url = delete_test_link + test_id;
	$.ajax({  
		type: "POST",
		url: url,
		data: null,
		success: function(data) {
			switch(data) {
				default:
					if (document.getElementById("t"+ test_id)) {
						document.getElementById("ti"+ test_id).innerHTML = 'Removed';
						document.getElementById("t"+ test_id).remove();
					}
					notify('Success!', 'Test Deleted Successfully', 'success');
					break;
				case 'error':
					notify('Error!', 'Test Deletion Failed', 'error');
					break;
			}
		},
	});
	return;
}
</script>
</section>