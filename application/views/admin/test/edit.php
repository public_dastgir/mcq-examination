<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	// Test Statistics
	$totalTestTaken = count($studentTest);
	$totalUniqueStudents = 0;
	$totalStartedTest = 0;
	$totalEndedTest = 0;
	$totalScoredTest = 0;
	$avgScore = 0;
	$maxScore = 0;
	$maxScoreBy = '';
	$minScore = 0;
	$minScoreBy = '';
	$avgPercent = 0;
	$maxPercent = 0;
	$minPercent = 0;
	$totalQuestions = $test[0]->questions;
	$mostTestName = '';
	$mostTestCount = 0;

	// Temp Array
	$students = [];
	$questionsAttempted = [];
	$mostTest = [];
	for ($i = 0; $i < $totalTestTaken; $i++) {
		$students[] = $studentTest[$i]->user_id;
		if (!isset($mostTest[$studentTest[$i]->student_name])) {
			$mostTest[$studentTest[$i]->student_name] = 1;
		} else {
			$mostTest[$studentTest[$i]->student_name]++;
		}

		if ($studentTest[$i]->status == 0)
			$totalStartedTest++;
		else
			$totalEndedTest++;

		if ($studentTest[$i]->score >= 0)
			$totalScoredTest++;

		$questionsAttempted[] = $studentTest[$i]->score;
		$avgScore += $studentTest[$i]->score;
		if ($maxScore < $studentTest[$i]->score) {
			$maxScore = $studentTest[$i]->score;
			$maxScoreBy = $studentTest[$i]->student_name;
		}
		if ($minScore > $studentTest[$i]->score) {
			$minScore = $studentTest[$i]->score;
			$minScoreBy = $studentTest[$i]->student_name;
		}
	}
	foreach ($mostTest as $key => $value) {
		if ($mostTestCount < $value) {
			$mostTestCount = $value;
			$mostTestName = $key;
		}
	}
	// Final Calculation
	if ($totalTestTaken > 0)
		$avgScore /= $totalTestTaken;
	$totalUniqueStudents = count(array_unique($students));
	if ($totalQuestions > 0) {
		$avgPercent = round($avgScore/$totalQuestions*100, 2);
		$maxPercent = round($maxScore/$totalQuestions*100, 2);
		$minPercent = round($minScore/$totalQuestions*100, 2);
	}
?>
<section class="content">
<!-- Test Statistics -->
<div class="row clearfix">
	<div class="col-xs-8">
		<div class="card">
			<div class="header bg-green">
				Test Settings (<?php echo $test[0]->name; ?>)
			</div>
			<div class="body">
				<form class="form_settings" onsubmit="return form_settings(<?php echo $id; ?>);">
					<div class="input-group form-group form-float">
						<div class="row">
							<!-- Added On -->
							<div class="col-xs-6">
								<div class="form-line disabled">
									<input type="text" class="form-control" placeholder="Added On: <?php echo $test[0]->added_on; ?>" disabled />
								</div>
							</div>
							<!-- Added By -->
							<div class="col-xs-6">
								<div class="form-line disabled">
									<input type="text" class="form-control" placeholder="Added By: <?php echo $test[0]->fname .' '. $test[0]->lname; ?>" disabled />
								</div>
							</div>
						</div>
						<div class="row">
							<!-- Start Date -->
							<div class="col-xs-6">
								Start:
								<div class="form-line">
									<input type="text" class="datetimepicker form-control" value="<?php echo date('d-m-Y H:i:s', $test[0]->date_start); ?>" name="date_start">

								</div>
							</div>
							<!-- End Date -->
							<div class="col-xs-6">
								End:
								<div class="form-line">
									<input type="text" class="datetimepicker form-control" value="<?php echo date('d-m-Y H:i:s', $test[0]->date_end); ?>" name="date_end">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- Timing -->
						<div class="col-xs-6">
							<div class="form-group form-float">
								<div class="form-line">
									<input min="1" max="120" type="number" name="time" value="<?php echo $test[0]->time; ?>" class="form-control">
									<label class="form-label">Time Limit(Minutes)</label>
								</div>
							</div>
						</div>
						<!-- Randomize? -->
						<div class="col-xs-6">
							<div class="form-group form-float">
								<input type="checkbox" id="randomize" name="randomize" class="chk-col-green" <?php if ($test[0]->randomize) echo 'checked'; ?> />
								<label for="randomize">Randomize Questions?</label>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- Branch -->
						<div class="col-xs-6">
							<div class="input-group form-group form-float">
								<span class="input-group-addon">
									<i class="material-icons">list</i>
								</span>
								<select class="form-line form-control selectpicker" data-live-search="true" name="branch">
									<option value="COMP" <?php if ($test[0]->branch == 'COMP') echo "selected"; ?>>Computer Engineering</option>
									<option value="CIVI" <?php if ($test[0]->branch == 'CIVI') echo "selected"; ?>>Civil Engineering</option>
									<option value="MECH" <?php if ($test[0]->branch == 'MECH') echo "selected"; ?>>Mechnanical Engineering</option>
									<option value="ELEC" <?php if ($test[0]->branch == 'ELEC') echo "selected"; ?>>Electronics Engineering</option>
									<option value="ETRX" <?php if ($test[0]->branch == 'ETRX') echo "selected"; ?>>Electronics and Telecommunications</option>
									<option value="BIOT" <?php if ($test[0]->branch == 'BIOT') echo "selected"; ?>>Biotechnology</option>
								</select>
							</div>
						</div>
						<!-- Year -->
						<div class="col-xs-6">
							<div class="input-group form-group form-float">
								<span class="input-group-addon">
									<i class="material-icons">date_range</i>
								</span>
								<select class="selectpicker form-control form-line" data-live-search="true" name="year">
									<option value="1" <?php if ($test[0]->year == 1) echo "selected"; ?>>F.E</option>
									<option value="2" <?php if ($test[0]->year == 2) echo "selected"; ?>>S.E</option>
									<option value="3" <?php if ($test[0]->year == 3) echo "selected"; ?>>T.E</option>
									<option value="4" <?php if ($test[0]->year == 4) echo "selected"; ?>>B.E</option>
								</select>
							</div>
						</div>
					</div>
					<button class="btn btn-block bg-green waves-effect" type="submit">Save Settings</button>
				</form>
			</div>
		</div>
	</div>
	<div class="col-xs-4">
		<div class="card">
			<div class="header bg-green">
				Test Statistics
			</div>
			<div class="body">
				Total Ongoing Test: <b><?php echo $totalStartedTest; ?></b><br/>
				Total Test Taken: <b><?php echo $totalTestTaken; ?></b><br/>
				Total Test Ended: <b><?php echo $totalEndedTest; ?></b><br/>
				Total Scored Test: <b><?php echo $totalScoredTest; ?></b><br/>
				Test Given by Unique Student: <b><?php echo $totalUniqueStudents; ?></b><br/>
				Most Test Given By: <b><?php echo $mostTestName; ?> (<?php echo $mostTestCount; ?>)</b><br/>
				Min Score: <b><?php echo $minScoreBy.': '.$minScore; ?> (<?php echo $minPercent; ?>%)</b><br/>
				Max Score: <b><?php echo $maxScoreBy.': '.$maxScore; ?> (<?php echo $maxPercent; ?>%)</b><br/>
				Avg Score: <b><?php echo ceil($avgScore); ?> (<?php echo $avgPercent; ?>%)</b><br/>
			</div>
		</div>
	</div>
<script>
	var link_settings = '<?php echo base_url();?>admin/test/settings/';
	function form_settings(test_id) {
		var url = link_settings + test_id;
		 $.ajax({  
			type: "POST",
			url: url,
			data: $('.form_settings').serialize(),
			success: function(data) {
				switch(data) {
					default:
						notify('Settings', 'Saved', 'success');
						break;
					case 'error':
						notify('Settings', 'Error Saving settings. Please check the form carefully.', 'danger');
						break;
				}
			},
		});
		return false;
	}
</script>
</div>
<!-- Quick Question Row -->
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="header bg-green">
				Add Quick Question
			</div>
			<div class="body">
				<div class="row">
					<!-- Question Category -->
					<div class="col-xs-6">
						<div class="input-group form-group form-float">
							<span class="input-group-addon">
								<i class="material-icons">list</i>
							</span>
							<select class="form-line form-control selectpicker" id="que_category" data-live-search="true" name="que_category">
								<?php
								for ($i = 0; $i < count($questionCategory); $i++) {
									echo '<option value="'. $questionCategory[$i]->id .'">'. $questionCategory[$i]->name .'</option>';
								}
								?>
							</select>
						</div>
					</div>
					<!-- How Many to add? -->
					<div class="col-xs-6">
						<div class="form-group form-float">
							<div class="form-line">
								<input min="1" max="120" type="number" id="que_add" name="que_add" class="form-control">
								<label class="form-label">How many questions to add?</label>
							</div>
						</div>
					</div>
				</div>
				<a class="btn btn-block btn-default waves-effect" type="submit" onclick="onAddQuestion(<?php echo $id; ?>)">Add Question</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var add_question_category = '<?php echo base_url();?>admin/test/add_question_category/';
	var test_obj;
	function onAddQuestion(test_id) {
		que_add = document.getElementById('que_add');
		que_category = document.getElementById('que_category');
		number = parseInt(que_add.value)
		name = que_category.selectedOptions[0].text
		if (number <= 0 || number >= 120) {
			notify('Test', 'Cannot add '+ number +' Questions', 'danger');
			return false;
		}
		var retVal = confirm('Are you sure you want to add Question?\nNumber of Questions to Add: '+ number +'\nCategory: '+ name);
		if (retVal == false)
			return false;
		
		 var url = add_question_category + test_id;
		 $.ajax({  
			type: "POST",
			url: url,
			data: {
				value: number,
				category: que_category.value
			},
			success: function(data) {
				switch(data) {
					default:
						var obj = $.parseJSON(data);
						for (var i = 0; i < obj.length; i++) {
							add_question_to_table(obj[i], obj[i]['question'], obj[i]['a'], obj[i]['b'], obj[i]['c'], obj[i]['d'], obj[i]['e'], obj[i]['f']);
						}
						que_add.value = 0;
						break;
					case 'error':
						notify('Test', 'Question could not be added, Please check the values carefully.', 'danger');
						break;
					case 'error_no_question':
						notify('Test', 'All Questions of this category are already added.', 'danger');
						break;
				}
			},
		});
		return true;
	}
</script>
<!-- Add Question Row -->
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="header bg-green">
				Add Questions
			</div>
			<form class="form_question" id="sign_in" onsubmit="return add_question(<?php echo $id;?>);">
				<div class="body">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="2" class="form-control" placeholder="Type Question here" required name="question"></textarea>
						</div>
						<div class="row">
							<br/>
							<?php
								for ($i = 1; $i <= 6; $i++) {
							?>
								<?php if ($i%2 == 1) { ?>
								<div class="col-sm-4 col-sm-offset-2">
								<?php } else { ?>
								<div class="col-sm-4 col-sm-offset-1">
								<?php } ?>
									<div class="form-group form-control">
										<input type="checkbox" id="q<?php echo $i; ?>" name="cb<?php echo $i; ?>" class="chk-col-green" />
										<label for="q<?php echo $i; ?>"><input type="text" class="form-control form-line" placeholder="Answer <?php echo $i; ?>" name="a<?php echo $i; ?>"/></label>
									</div>
								</div>
							<?php
								}
							?>
						</div>
					</div>
					<div class="row">
						<strong>Note:</strong> Leave the Answer field unfilled if there's no more option to give.<br/>
						<strong>Note:</strong> Tick the Correct Answer <i>Atleast 1 should be ticked</i><br/>
					</div>
					<div class="row">
						<div class="col-sm-offset-4 col-sm-4">
							<button class="btn btn-block bg-green waves-effect" type="submit">Add Question</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	var c = 0;
	var link_request = '<?php echo base_url();?>admin/test/add_question/';

	function add_question_to_table(obj, question, a1, a2, a3, a4, a5, a6) {
		// obj requires id/correct
		if(document.getElementById("questions")) {
			var a = '<tr id="qt'+ obj['id'] +'" role="row" class="even">'+
					'<td class="sorting-1">'+ obj['id'] +'</td>'+
					'<td id="qti'+ obj['id'] +'">'+ question +'</td>'+
					'<td>'+ a1 + ((obj['correct']&1)?'<i class="material-icons">check</i>':'') +'</td>'+
					'<td>'+ a2 + ((obj['correct']&2)?'<i class="material-icons">check</i>':'') +'</td>'+
					'<td>'+ a3 + ((obj['correct']&4)?'<i class="material-icons">check</i>':'') +'</td>'+
					'<td>'+ a4 + ((obj['correct']&8)?'<i class="material-icons">check</i>':'') +'</td>'+
					'<td>'+ a5 + ((obj['correct']&16)?'<i class="material-icons">check</i>':'') +'</td>'+
					'<td>'+ a6 + ((obj['correct']&32)?'<i class="material-icons">check</i>':'') +'</td>'+
					'<td><div class="btn-group">'+
						'	<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
						'		Actions <span class="caret"></span>'+
						'	</button>'+
						'	<ul class="dropdown-menu">'+
						'		<li><a href="<?php echo base_url();?>admin/question/edit/'+ obj['id'] +'">Edit</a></li>'+
						'		<li><a href="javascript:void(0);" onclick="deleteQuestion('+ obj['id'] +');">Delete</a></li>   <!-- @todo Alert Box -->'+
						'	</ul>'+
					'</div></td>'+
					'</tr>';
			$("table#questions").append(a);
			if (c == 0) {
				c = 1;
				notify('Test Info', 'Question Added Successfully.<br>New Questions would be added to Table Automatically<br>Refresh the Page to use Search Function for Question Table', 'success')
			}
		} else if (c == 0) {
			c = 1;
			notify('Test Info', 'Question Added Successfully.<br>Refresh the Page to see the Question Table below', 'success')
		}
	}


	function add_question(test_id) {
		var url = link_request + test_id;
		 $.ajax({  
			type: "POST",
			url: url,
			data: $('.form_question').serialize(),
			success: function(data) {
				switch(data) {
					default:
						var obj = $.parseJSON(data);
						add_question_to_table(obj, $('.form_question').find('textarea[name="question"]').val(), $('.form_question').find('input[name="a1"]').val(), $('.form_question').find('input[name="a2"]').val(), $('.form_question').find('input[name="a3"]').val(), $('.form_question').find('input[name="a4"]').val(), $('.form_question').find('input[name="a5"]').val(), $('.form_question').find('input[name="a6"]').val());
						$('.form_question').trigger("reset"); // Reset Form
						break;
					case 'error':
						notify('Test Info', 'Question could not be added, Please check the form carefully.', 'danger')
						break;
				}
			},
		});
		return false;
	}
</script>
<?php if (isset($questions) && count($questions)) { ?>
<!-- List Questions -->
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="table-responsive">
				<div class="header bg-green">
					Question List
				</div>
				<div class="body">
					<table id="questions" class="table table-bordered table-striped table-hover js-basic-example dataTable">
						<thead>
							<tr>
								<th>Question#</th>
								<th>Question</th>
								<th>A</th>
								<th>B</th>
								<th>C</th>
								<th>D</th>
								<th>E</th>
								<th>F</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Question#</th>
								<th>Question</th>
								<th>A</th>
								<th>B</th>
								<th>C</th>
								<th>D</th>
								<th>E</th>
								<th>F</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							<?php
							for ($i = 0; $i < count($questions); $i++) {
								echo '<tr id="qt'. $questions[$i]->id .'">';
									$questions[$i]->correct = intval($questions[$i]->correct);
									echo '<td id="qti'. $questions[$i]->id .'">'. ($i+1) .'</td>';
									echo '<td>'. $questions[$i]->question .'</td>';
									echo '<td>'. $questions[$i]->a . (($questions[$i]->correct&1)?'<i class="material-icons">check</i>':'') .'</td>';
									echo '<td>'. $questions[$i]->b . (($questions[$i]->correct&2)?'<i class="material-icons">check</i>':'') .'</td>';
									echo '<td>'. $questions[$i]->c . (($questions[$i]->correct&4)?'<i class="material-icons">check</i>':'') .'</td>';
									echo '<td>'. $questions[$i]->d . (($questions[$i]->correct&8)?'<i class="material-icons">check</i>':'') .'</td>';
									echo '<td>'. $questions[$i]->e . (($questions[$i]->correct&16)?'<i class="material-icons">check</i>':'') .'</td>';
									echo '<td>'. $questions[$i]->f . (($questions[$i]->correct&32)?'<i class="material-icons">check</i>':'') .'</td>';
									echo '<td>';
							?>
										<div class="btn-group">
											<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Actions <span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="<?php echo base_url();?>admin/test/edit_question/<?php echo $questions[$i]->id; ?>">Edit</a></li>
												<li><a href="javascript:void(0);" onclick="deleteQuestion(<?php echo $questions[$i]->id; ?>);">Delete</a></li>   <!-- @todo Alert Box -->
											</ul>
										</div>
							<?php
									echo '</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<strong>Note(On Dynamic Insert/Delete Entries):</strong><br/>
					<i> Table is populated without refresh, however, the data might not be accurate due to following reasons</i><br/>
					<i> 1) Data might be filtered out on server </i><br/>
					<i> 2) Dynamic Data are fetched from browser and shown, and no connection to server is made to validate it </i><br/>
					<i> If you want to use search functionality(with correct rows), Please refresh the browser</i>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var delete_link = '<?php echo base_url();?>admin/test/remove_question/';
	function deleteQuestion(question_id) {
		var url = delete_link + question_id;
		$.ajax({  
			type: "POST",
			url: url,
			data: $('form').serialize(),
			success: function(data) {
				switch(data) {
					default:
						if(document.getElementById("qt"+ question_id)) {
							document.getElementById("qti"+ question_id).innerHTML = 'Removed';
							document.getElementById("qt"+ question_id).remove();
						}
						break;
					case 'error':
						break;
				}
			},
		});
		return false;
	}
</script>
<?php } ?>
</section>