<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="header bg-green">
				<h2>New Test</h2>
			</div>
			<div class="body">
				<form id="wizard_with_validation" method="POST" action='<?php echo base_url();?>sign_in'>
					<h3>Test Information</h3>
					<fieldset>
						<div class="form-group form-float">
							<div class="form-line">
								<input type="text" name="name" class="form-control" required>
								<label class="form-label">Test Name</label>
							</div>
						</div>
						<div class="input-group form-group form-float">
							<span class="input-group-addon">
								<i class="material-icons">date_range</i>
							</span>
							<select class="selectpicker form-control form-line" data-live-search="true" name="year" required>
								<option value="1">F.E</option>
								<option value="2">S.E</option>
								<option value="3">T.E</option>
								<option value="4">B.E</option>
							</select>
						</div>
						<div class="input-group form-group form-float">
							<span class="input-group-addon">
								<i class="material-icons">list</i>
							</span>
							<select class="form-line form-control selectpicker" data-live-search="true" name="branch" required>
								<option value="COMP">Computer Engineering</option>
								<option value="CIVI">Civil Engineering</option>
								<option value="MECH">Mechnanical Engineering</option>
								<option value="ELEC">Electronics Engineering</option>
								<option value="ETRX">Electronics and Telecommunications</option>
								<option value="BIOT">Biotechnology</option>
							</select>
						</div>
					</fieldset>

					<h3>Timing</h3>
					<fieldset>
						<div class="input-group form-group form-float">
							Date to Start:<br/>
							<span class="input-group-addon">
								<i class="material-icons">date_range</i>
							</span>
							<div class="form-line">
								<input type="text" class="datetimepicker form-control" placeholder="Date to start the test" name="date_start">

							</div>
						</div>
						<div class="input-group form-group form-float">
							Date to End:<br/>
							<span class="input-group-addon">
								<i class="material-icons">date_range</i>
							</span>
							<div class="form-line">
								<input type="text" class="datetimepicker form-control" placeholder="Date to End the test" name="date_end">

							</div>
						</div>
						<div class="form-group form-float">
							<div class="form-line">
								<input min="1" max="120" type="number" name="time" class="form-control" required>
								<label class="form-label">Time limit(in minutes)</label>
							</div>
						</div>
					</fieldset>
				</div>
			</form>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
var finish_post_url = '<?php echo base_url(); ?>admin/test/add';
var redirect_url = '<?php echo base_url(); ?>admin/test/list';
</script>