<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
	$totalTestTaken = count($studentTest);
	$totalUniqueStudents = 0;
	$totalStartedTest = 0;
	$totalEndedTest = 0;
	$totalScoredTest = 0;
	$avgScore = 0;
	$maxScore = 0;
	$maxScoreBy = '';
	$minScore = INF;
	$minScoreBy = '';
	$avgPercent = 0;
	$maxPercent = 0;
	$minPercent = 0;
	$totalQuestions = $tests[0]->questions;
	$mostTestName = '';
	$mostTestCount = 0;

	// Temp Array
	$students = [];
	$questionsAttempted = [];
	$mostTest = [];
	for ($i = 0; $i < $totalTestTaken; $i++) {
		$students[] = $studentTest[$i]->user_id;
		if (!isset($mostTest[$studentTest[$i]->student_name])) {
			$mostTest[$studentTest[$i]->student_name] = 1;
		} else {
			$mostTest[$studentTest[$i]->student_name]++;
		}

		if ($studentTest[$i]->status == 0)
			$totalStartedTest++;
		else
			$totalEndedTest++;

		if ($studentTest[$i]->score >= 0)
			$totalScoredTest++;

		$questionsAttempted[] = $studentTest[$i]->score;
		$avgScore += $studentTest[$i]->score;
		 if ($maxScore < $studentTest[$i]->score) {
			$maxScore = $studentTest[$i]->score;
			$maxScoreBy = $studentTest[$i]->student_name;
		}
		if ($minScore > $studentTest[$i]->score) {
			$minScore = $studentTest[$i]->score;
			$minScoreBy = $studentTest[$i]->student_name;
		}
	}
	foreach ($mostTest as $key => $value) {
		if ($mostTestCount < $value) {
			$mostTestCount = $value;
			$mostTestName = $key;
		}
	}
	// Final Calculation
	if ($totalTestTaken > 0)
		$avgScore /= $totalTestTaken;
	$totalUniqueStudents = count(array_unique($students));
	if ($totalQuestions > 0) {
		$avgPercent = round($avgScore/$totalQuestions*100, 2);
		$maxPercent = round($maxScore/$totalQuestions*100, 2);
		$minPercent = round($minScore/$totalQuestions*100, 2);
	}
?>
<section class="content">
<div class="row clearfix">
	<div class="col-xs-4">
		<div class="card">
			<div class="header bg-green">
				Text Statistics
			</div>
			<div class="body">
				Test Name: <b><?php echo $tests[0]->name; ?></b><br/>
				Total Ongoing Test: <b><?php echo $totalStartedTest; ?></b><br/>
				Total Test Ended: <b><?php echo $totalEndedTest; ?></b><br/>
				Total Scored Test: <b><?php echo $totalScoredTest; ?></b><br/>
			</div>
		</div>
	</div>
	<div class="col-xs-4">
		<div class="card">
			<div class="header bg-green">
				Text Statistics
			</div>
			<div class="body">
				Total Test Taken: <b><?php echo $totalTestTaken; ?></b><br/>
				Test Given by Unique Student: <b><?php echo $totalUniqueStudents; ?></b><br/>
				Most Test Given By: <b><?php echo $mostTestName; ?> (<?php echo $mostTestCount; ?>)</b><br/>
			</div>
		</div>
	</div>
	<div class="col-xs-4">
		<div class="card">
			<div class="header bg-green">
				Text Statistics
			</div>
			<div class="body">
				Min Score: <b><?php echo $minScoreBy.': '.$minScore; ?> (<?php echo $minPercent; ?>%)</b><br/>
				Max Score: <b><?php echo $maxScoreBy.': '.$maxScore; ?> (<?php echo $maxPercent; ?>%)</b><br/>
				Avg Score: <b><?php echo ceil($avgScore); ?> (<?php echo $avgPercent; ?>%)</b><br/>
			</div>
		</div>
	</div>
</div>
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="header bg-green">
				Student and Score
			</div>
			<div class="body">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover js-basic-example dataTable"  data-order='[[ 5, "desc" ]]'>
						<thead>
							<tr>
								<th>UIN</th>
								<th>Student Name</th>
								<th>Score</th>
								<th>Percent</th>
								<th>Status</th>
								<th>Date</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>UIN</th>
						  		<th>Student Name</th>
								<th>Score</th>
								<th>Percent</th>
								<th>Status</th>
								<th>Date</th>
							</tr>
						</tfoot>
						<tbody>
							<?php
								for ($i = 0; $i < count($studentTest); $i++) {
									if ($studentTest[$i]->status == 1) {
										$status = 'Ended';
									} else {
										$status = 'Started';
									}
									$percent = round($studentTest[$i]->score/$totalQuestions*100, 2);
									echo '<tr>';
										echo '<td>'. $studentTest[$i]->uin .'</td>';
										echo '<td>'. $studentTest[$i]->student_name .'</td>';
										echo '<td>'. $studentTest[$i]->score .'</td>';
										echo '<td>'. $percent .'%</td>';
										echo '<td>'. $status .'</td>';
										echo '<td>'. date('Y-m-d H:i:s', $studentTest[$i]->time_start) .'</td>';
									echo '</tr>';
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row clearfix">
	<div class="col-xs-12">
		<div class="card">
			<div class="header bg-green">
				Graph
			</div>
			<div class="body">
				<div class="row">
					<div class="col-xs-6">
						Number of Tests Started Per Day:
						<div id="test_chart" class="graph"></div>
					</div>
					<div class="col-xs-6">
						Recent Scores:
						<div id="score_chart" class="graph"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if (isset($studentTest) && $studentTest != null) { ?>
<script>
document.addEventListener('DOMContentLoaded', function() {
	<?php
	$branch = ['COMP', 'CIVI', 'MECH', 'ELEC', 'ETRX', 'BIOT'];
	$period_test = [];
	$period_question = [];
	for ($i = 0; $i < count($studentTest); $i++) {
		$idx = date('H', strtotime($studentTest[$i]->time_start));
		if (!isset($period_test[$idx])) {
			$period_test[$idx] = 0;
			$period_testb[$idx] = []; 
			for ($j = 0; $j < count($branch); $j++) {
				$period_testb[$idx][$branch[$j]] = 0; 
			}
		}
		$period_test[$idx] += 1;
		$period_testb[$idx][$studentTest[$i]->branch]++;
	}
	?>
	Morris.Line({
		element: 'test_chart',
		data: [
		<?php
			foreach ($period_test as $date => $value) {
				echo '{';
				echo 'period: \''. $date .'\',';
					echo 'no: '. $value .',';
				foreach ($period_testb[$date] as $branch => $test_count) {
					if ($test_count == 0) {
						echo $branch .': null,';
					} else {
						echo $branch .': '. $test_count .',';
					}
				}
				echo '},';
			}
		?>
		],
		parseTime: false,
		xkey: 'period',
		ykeys: ['COMP', 'CIVI', 'MECH', 'ELEC', 'ETRX', 'BIOT', 'no'],
		labels: ['Computer', 'Civil', 'Mechanical', 'Electrical', 'ETRX', 'BioTechnology', 'Total'],
		lineColors: ['#ef9a9a', '#f48fb1', '#ce93d8', '#ff1744', '#f50057', '#d500f9', '#c50057'],
		hideHover: 'auto',
	});
	Morris.Line({
		element: 'score_chart',
		data: [
		<?php
			for ($i = 0; $i < count($studentTest); $i++) {
				echo '{';
					echo 'period: \''. ($i+1) .'\',';
					echo 'score: \''. $studentTest[$i]->score .'\',';
					echo 'percent: \''. round($studentTest[$i]->score/$totalQuestions*100) .'%\',';
					
				echo '},';
			}
		?>
		],
		parseTime: false,
		xkey: 'period',
		ykeys: ['score', 'percent'],
		labels: ['Score', 'Percentage'],
		hideHover: 'auto',
		lineColors: ['#ef9a9a', '#d500f9']
	});
});
</script>
<?php } ?>
</section>