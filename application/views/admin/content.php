<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
<div class="row">
	<div class="col-xs-12">
		<!-- Add Test -->
		<div class="card">
			<div class="header bg-green">
				<h2>Test Management</h2>
			</div>
			<div class="body">
				<button onclick="location.href='<?php echo base_url(); ?>admin/test/add'" class="btn bg-green waves-effect col-xs-offset-3">Add/Schedule Test</button>
				<button onclick="location.href='<?php echo base_url(); ?>admin/test/list'" class="btn bg-green waves-effect col-xs-offset-3">Manage Test</button>
				<br/>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<!-- Test Management -->
		<div class="card">
			<div class="header bg-blue">
				<h2>Test Statistics</h2>
			</div>
			<div class="body">
				<div class="row">
					<div class="col-xs-6">
						Number of Tests Added Per Month:
						<div id="test_chart" class="graph"></div>
					</div>
					<div class="col-xs-6">
						Number of Questions Added Per Month:
						<div id="question_chart" class="graph"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if (isset($tests) && $tests != null) { ?>
<script>
document.addEventListener('DOMContentLoaded', function() {
	<?php
	$branch = ['COMP', 'CIVI', 'MECH', 'ELEC', 'ETRX', 'BIOT'];
	$period_test = [];
	$period_question = [];
	for ($i = 0; $i < count($tests); $i++) {
		$idx = date('M Y', strtotime($tests[$i]->added_on));
		if (!isset($period_test[$idx])) {
			$period_test[$idx] = []; 
			$period_question[$idx] = []; 
			for ($j = 0; $j < count($branch); $j++) {
				$period_test[$idx][$branch[$j]] = 0; 
				$period_question[$idx][$branch[$j]] = 0; 
			}
		}
		$period_test[$idx][$tests[$i]->branch] += 1;
		$period_question[$idx][$tests[$i]->branch] += $tests[$i]->questions;
	}
	?>
	Morris.Area({
		element: 'test_chart',
		data: [
		<?php
			foreach ($period_test as $date => $value) {
				echo '{';
				echo 'period: \''. $date .'\',';
				foreach ($value as $branch => $test_count) {
					if ($test_count == 0) {
						echo $branch .': null,';
					} else {
						echo $branch .': '. $test_count .',';
					}
				}
				echo '},';
			}
		?>
		],
		parseTime: false,
		xkey: 'period',
		ykeys: ['COMP', 'CIVI', 'MECH', 'ELEC', 'ETRX', 'BIOT'],
		labels: ['Computer', 'Civil', 'Mechanical', 'Electrical', 'ETRX', 'BioTechnology'],
		pointSize: 2,
		hideHover: 'auto',
		lineColors: ['#ef9a9a', '#f48fb1', '#ce93d8', '#ff1744', '#f50057', '#d500f9']
	});
	Morris.Area({
		element: 'question_chart',
		data: [
		<?php
			foreach ($period_question as $date => $value) {
				echo '{';
				echo 'period: \''. $date .'\',';
				foreach ($value as $branch => $test_count) {
					if ($test_count == 0) {
						echo $branch .': null,';
					} else {
						echo $branch .': '. $test_count .',';
					}
				}
				echo '},';
			}
		?>
		],
		parseTime: false,
		xkey: 'period',
		ykeys: ['COMP', 'CIVI', 'MECH', 'ELEC', 'ETRX', 'BIOT'],
		labels: ['Computer', 'Civil', 'Mechanical', 'Electrical', 'ETRX', 'BioTechnology'],
		pointSize: 2,
		hideHover: 'auto',
		lineColors: ['#ef9a9a', '#f48fb1', '#ce93d8', '#ff1744', '#f50057', '#d500f9']
	});
});
</script>
<?php } ?>
</section>