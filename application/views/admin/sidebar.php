<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="sidebar">
	<br/>
	<div class="menu">
		<ul class="list">
			<?php /* if ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == '') { */ ?>
			<li class="active">
				<h6 align="center">Test Management</h6>
				<a href="<?php echo base_url(); ?>admin/test/add">Add New Test</a>
				<a href="<?php echo base_url(); ?>admin/test/list">List Tests</a>
				<hr>
				<h6 align="center">Profile Management</h6>
				<a href="<?php echo base_url(); ?>admin">Admin Home Page</a>
				<a href="<?php echo base_url(); ?>profile">Student Home Page</a>
				<hr>
				<a href="<?php echo base_url(); ?>profile/logs">View Logs</a>
			</li>
		</ul>
	</div>
</section>