<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body class="signup-page">
	<div class="signup-box">
		<div class="logo">
			<a href="javascript:void(0);">Aptitude</a>
			<small>Rizvi College of Engineering</small>
		</div>
		<div class="card">
			<div class="body">
				<form id="sign_up" method="POST">
					<div class="msg">Register</div>
					 <?php if ($this->session->flashdata('error') != ''): ?>
					<div class="alert alert-danger">
						<?=$this->session->flashdata('error')?>
					</div>
					<?php endif; ?>
					<?php if ($this->session->flashdata('info') != ''): ?>
					<div class="alert alert-success">
						<?=$this->session->flashdata('info')?>
					</div>
					<?php endif; ?>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="uname" placeholder="Username" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="fname" placeholder="Name" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="lname" placeholder="Last Name" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">email</i>
						</span>
						<div class="form-line">
							<input type="email" class="form-control" name="email" placeholder="Email Address" required>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">date_range</i>
						</span>
						<select class="selectpicker form-line" data-live-search="true" name="year">
							<option value="1">F.E</option>
							<option value="2">S.E</option>
							<option value="3">T.E</option>
							<option value="4">B.E</option>
						</select>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">list</i>
						</span>
						<select class="selectpicker form-line" data-live-search="true" name="branch">
							<option value="COMP">Computer Engineering</option>
							<option value="CIVI">Civil Engineering</option>
							<option value="MECH">Mechnanical Engineering</option>
							<option value="ELEC">Electronics Engineering</option>
							<option value="ETRX">Electronics and Telecommunications</option>
							<option value="BIOT">Biotechnology</option>
						</select>
					</div>
					 <div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">code</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="uin" minlength="7" placeholder="UIN Number" required>
						</div>
					</div>
					<div class="form-group">
						<input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
						<label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
					</div>

					<button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">SIGN UP</button>

					<div class="m-t-25 m-b--5 align-center">
						<a href="<?php echo base_url();?>sign_in">You already have a membership?</a>
					</div>
				</form>
			</div>
		</div>
	</div>
