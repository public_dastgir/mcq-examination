<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body class="login-page">
	<div class="login-box">
		<div class="logo">
			<a href="javascript:void(0);">Aptitude</a>
			<small>Rizvi College of Engineering</small>
		</div>
		<div class="card">
			<div class="body" id="login_form">
				<form id="sign_in" method="POST" action='<?php echo base_url();?>sign_in'>
					<div class="msg">Sign In</div>
					<?php if ($this->session->flashdata('error') != ''): ?>
					<div class="alert alert-danger">
						<?=$this->session->flashdata('error')?>
				  	</div>
				  	<?php endif; ?>
					<?php if ($this->session->flashdata('info') != ''): ?>
					<div class="alert alert-success">
						<?=$this->session->flashdata('info')?>
					</div>
					<?php endif; ?>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">person</i>
						</span>
						<div class="form-line">
							<input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
						</div>
					</div>
					<div class="input-group">
						<span class="input-group-addon">
							<i class="material-icons">lock</i>
						</span>
						<div class="form-line">
							<input type="password" class="form-control" name="password" placeholder="Password" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
						</div>
					</div>
					<div class="row m-t-15 m-b--20">
						<div class="col-xs-6">
							<a href="<?php echo base_url();?>sign_in/register">Register Now!</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
