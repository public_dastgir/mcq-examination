<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	function load_js($constant, $folder, $js_file, $jsFolder = false) {
		global $load_js_constant;
		if ($jsFolder) {
			$jsFolderName = 'js';
		} else {
			$jsFolderName = 'plugins';
		}
		if ($constant >= 0 || $load_js_constant&$constant != 0) {
			echo '<script src="'. asset_url() .''. $jsFolderName .'/'. $folder .'/'. $js_file .'.js"></script>';
		}
	}
	/**
	 * Free Constants:
	 * 0x2
	 */
	
	load_js(0, "", "helpers", true);

	/* Input Mask Plugin Js */
	load_js(0, "jquery-inputmask", "jquery.inputmask.bundle");

	/* Bootstrap Core Js */
	load_js(0, "bootstrap/js", "bootstrap");

	/* Notify Plugin Js */
	load_js(0, "bootstrap-notify", "bootstrap-notify");

	/* Select Plugin Js */
	load_js(0x4, "bootstrap-select/js", "bootstrap-select");

	/* Custom Js */
	load_js(0x8, "pages/forms", "form-wizard", true);

	/* Jquery Validation Plugin Css */
	load_js(0x10, "jquery-validation", "jquery.validate");

	/* JQuery Steps Plugin Js */
	load_js(0x20, "jquery-steps", "jquery.steps");

	/* Slimscroll Plugin Js */
	load_js(0x40, "jquery-slimscroll", "jquery.slimscroll");

	/* Waves Effect Plugin Js */
	load_js(0x80, "node-waves", "waves");

	/* Jquery CountTo Plugin Js */
	load_js(0x100, "jquery-countto", "jquery.countTo");

	/* Sweet Alert Plugin Js */
	load_js(0x200, "sweetalert", "sweetalert.min");

	/* Moment Plugin Js */
	load_js(0x400, "momentjs", "moment");

	load_js(0x800, "bootstrap-material-datetimepicker/js", "bootstrap-material-datetimepicker");

	/* Morris Plugin Js */
	load_js(0x1000, "raphael", "raphael.min");
	load_js(0x2000, "morrisjs", "morris");


	/* Jquery DataTable Plugin Js */
	load_js(0x4000, "jquery-datatable", "jquery.dataTables");
	load_js(0x4000, "jquery-datatable/skin/bootstrap/js", "dataTables.bootstrap");
	load_js(0x4000, "jquery-datatable/extensions/export", "dataTables.responsive");
	load_js(0x4000, "jquery-datatable/extensions/export", "dataTables.buttons.min");
	load_js(0x4000, "jquery-datatable/extensions/export", "buttons.flash.min");
	load_js(0x4000, "jquery-datatable/extensions/export", "buttons.html5.min");
	load_js(0x4000, "jquery-datatable/extensions/export", "buttons.print.min");
	load_js(0x8000, "jquery-datatable/extensions/export", "jszip.min");
	load_js(0x10000, "jquery-datatable/extensions/export", "pdfmake.min");
	load_js(0x10000, "jquery-datatable/extensions/export", "vfs_fonts");

	/* Demo Js */
	load_js(0, "", "demo", true);
	load_js(0, "", "admin", true);
	load_js(0x8, "pages/forms", "basic-form-elements", true);
	load_js(0x4000, "pages/tables", "jquery-datatable", true);
	load_js(0x20000, "pages/ui", "tooltips-popovers", true);
?>
</body>

<!--
Made by Dastgir Pojee.
-->
</html>