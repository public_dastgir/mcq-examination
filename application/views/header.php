<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title><?=$title?></title>
	<!-- Favicon-->
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

	<!-- Bootstrap Core Css -->
	<link href="<?= asset_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Waves Effect Css -->
	<link href="<?= asset_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

	<!-- Animation Css -->
	<link href="<?= asset_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

	<!-- Morris Chart Css-->
	<link href="<?= asset_url();?>plugins/morrisjs/morris.css" rel="stylesheet" />

	<!-- Custom Css -->
	<link href="<?= asset_url();?>css/style.css" rel="stylesheet">

	<link href="<?= asset_url();?>css/themes/all-themes.css" rel="stylesheet" />

	<link href="<?= asset_url();?>plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

	<!-- Sweet Alert Css -->
	<link href="<?= asset_url();?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

	<!-- Dropzone Css -->
	<link href="<?= asset_url();?>plugins/dropzone/dropzone.css" rel="stylesheet">

	<link href="<?= asset_url();?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

	<!-- JQuery DataTable Css -->
	<link href="<?= asset_url();?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
	<link href="<?= asset_url();?>plugins/jquery-datatable/responsive.bootstrap.css" rel="stylesheet">

	<!-- Jquery Core Js -->
	<script src="<?= asset_url();?>plugins/jquery/jquery.min.js"></script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-70809546-3', 'auto');
		ga('send', 'pageview');

	</script>
</head>