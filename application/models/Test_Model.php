<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Test_Model extends CI_Model {
	/**
	 * Adds Test Entry from POST Request and returns the testID
	 * @method add_test_entry
	 */
	public function add_test_entry()
	{
		$start = strtotime($this->input->post('date_start'));
		$end = strtotime($this->input->post('date_end'));
		$time = intval($this->input->post('time'));
		if ($end <= $start || ($end - $start) < ($time * 60)) {
			return 0;
		}
		$name = $this->input->post('name');
		$data = array(
			'name' => $name,
			'year' => $this->input->post('year'),
			'branch' => $this->input->post('branch'),
			'date_start' => $start,
			'date_end' => $end,
			'time' => $time,
			'added_by' => $this->session->userdata('id'),
		);
		
		// Insert
		if ($this->db->insert('test_main', $data)) {
			$id = $this->db->insert_id();
			$this->sql_log->info('Test '. $id .'('. $name .') Added');
			return $id;
		}
		return 0;
	}

	/**
	 * Checks if $id Test Exists
	 * @method exists
	 * @param  int	$id	Test ID
	 * @return bool		True if exists
	 */
	public function exists($id)
	{
		$this->db->where('id', $id);

		$query = $this->db->get('test_main');

		if ($query->result_id->num_rows == 1) {
			$row = $query->row();
			return true;
		}
		
		return false;
	}

	/**
	 * Generates Test List and returns it
	 * @method test_list
	 * @param  int		$test_id	Test ID
	 * @param  string	$branch		Branch Name
	 * @param  string	$year		Year (1/2//4)
	 * @return array				Array contains object of row.
	 */
	public function test_list($test_id = 0, $branch = null, $year = null)
	{
		//SELECT `test_main`.*, `users`.`fname`, `users`.`lname`, count(test_questions.test_id) AS questions FROM `test_main` LEFT JOIN `users` ON `users`.`id` = `test_main`.`added_by` LEFT JOIN `test_questions` ON `test_main`.`id`=`test_questions`.`test_id` group by `test_main`.`id`
		$this->db->select('test_main.*, users.fname, users.lname, count(test_questions.test_id) AS questions')
				->from('`test_main`')
				->join('users', 'users.id = test_main.added_by', 'left')
				->join('test_questions', '`test_main`.`id`=`test_questions`.`test_id`', 'left')
				->group_by('`test_main`.`id`');
		if ($test_id > 0) {
			$this->db->where('test_main.id', $test_id);
		}
		if ($branch != null) {
			$this->db->where('test_main.branch', $branch);
		}
		if ($year != null) {
			$this->db->where('test_main.year', $year);
		}
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			$rw = $query->result();
			return $rw;
		}
		return NULL;
	}

	/**
	 * Returns All Questions added in $id Test
	 * @method question_list
	 * @param  int		id			Test ID
	 * @param  boolean	randomize	Randomize the Output?(not used)
	 * @return array
	 */
	public function question_list($testId, $randomize = false)
	{
		// test_questions(id, test_id, question, a, b, c, d, e, f, correct)
		$this->db->from('test_questions')
				->where('test_id', $testId);
		if ($randomize == true) {
			$this->db->order_by('id', 'random');
		}
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			$rw = $query->result();
			return $rw;
		}
		return NULL;
	}

	/**
	 * Adds Question to Database and return QuestionID
	 * @method add_question
	 * @param  int	$test_id	TestID
	 */
	public function add_question($test_id)
	{
		if (!$this->exists($test_id))
			return null;
		$answers = [];
		$cb = 0;
		for ($i = 1; $i <= 6; $i++) {
			$answers[$i-1] = '';
		}
		for ($i = 1; $i <= 6; $i++) {
			if (empty($this->input->post('a'. $i)))
				break;
			if (!empty($this->input->post('cb'. $i)) && $this->input->post('cb'. $i) == 'on') {
				$cb += 1 << ($i - 1);
			}
			$answers[$i-1] = $this->input->post('a'. $i);
		}
		if ($cb == 0) {
			return null;
		}
		// Data to insert
		$data = array(
			'test_id' => $test_id,
			'question' => $this->input->post('question'),
			'a' => $answers[0],
			'b' => $answers[1],
			'c' => $answers[2],
			'd' => $answers[3],
			'e' => $answers[4],
			'f' => $answers[5],
			'correct' => $cb,
		);
		
		// Insert
		if ($this->db->insert('test_questions', $data)) {
			$id = $this->db->insert_id();
			$this->sql_log->info('Question '. $id .' in Test#'. $test_id .' Added by '. $this->session->userdata('name'));
			return ['id' => $id, 'correct' => $cb];
		}
		return null;
	}

	/**
	 * Checks if $questionId Question Exist with correct user.
	 * @method exists
	 * @param	int	$questionId	Question ID
	 * @param	int	$user		UserID
	 * @return	bool			True if exists
	 */
	public function question_exists($questionId, $user = null)
	{
		$this->db->where('test_questions.id', $questionId);
		$this->db->from('test_questions');
		if ($user != null) {
			$this->db->select('test_questions.*')
					->join('test_main', 'test_main.id = test_questions.test_id')
					->join('users', 'users.id = test_main.added_by');
		}
		$query = $this->db->get();

		if ($query->result_id->num_rows == 1) {
			$row = $query->row();
			return true;
		}
		
		return false;
	}

	/**
	 * Removes Particular Question
	 * @method remove_question
	 * @param  int	$questionId	Question ID
	 * @return int				1 if removed, else 0
	 */
	public function remove_question($questionId)
	{
		$this->db->where('id', $questionId);
		$this->db->delete('test_questions');
		if ($this->db->affected_rows()) {
			$this->sql_log->info('Question '. $questionId .' Deleted by '. $this->session->userdata('name'));
			return 1;
		}
		return 0;
	}

	/**
	 * Saves Settings
	 * @method save_settings
	 * @param  int	$testID	Test ID
	 * @return int			1 if removed, else 0
	 */
	public function save_settings($testId)
	{
		$start = strtotime($this->input->post('date_start'));
		$end = strtotime($this->input->post('date_end'));
		$time = intval($this->input->post('time'));
		if ($end <= $start || ($end - $start) < ($time * 60)) {
			return 0;
		}
		if ($this->input->post('randomize') == 'on') {
			$random = 1;
		} else {
			$random = 0;
		}
		$data = array(
			'year' => $this->input->post('year'),
			'branch' => $this->input->post('branch'),
			'date_start' => $start,
			'date_end' => $end,
			'time' => $time,
			'randomize' => $random,
		);
		
		// Update
		$this->db->where('id', $testId);
		$this->db->update('test_main', $data);
		if ($this->db->affected_rows()) {
			$this->sql_log->info('Test '. $testId .' Settings Updated by '. $this->session->userdata('name'));
			return 1;
		}
		return 0;
	}

	/**
	 * Removes Particular Test and it's Questions
	 * @method remove
	 * @param  int	$testId	Test ID
	 * @return int			1 if removed, else 0
	 */
	public function remove($testId)
	{
		$this->db->where('id', $testId);
		$this->db->delete('test_main');

		if ($this->db->affected_rows()) {
			$this->sql_log->info('Test '. $testId .' Deleted by '. $this->session->userdata('name'));

			$this->db->where('test_id', $testId);
			$this->db->delete('test_questions');
			$count = $this->db->affected_rows();
			if ($count) {
				$this->sql_log->info('Questions('. $count .') of Test#'. $testId .' Deleted by '. $this->session->userdata('name'));
			}
			return 1;
		}
		return 0;
	}

	/** Student Functions */

	/**
	 * Checks if user is eligible for given test id
	 * @method eligible
	 * @param  int		$testId			test id
	 * @param  boolean	$checkEndDate	Check if test time has ended?
	 * @return boolean					true if eligible.
	 */
	public function eligible($testId, $checkEndDate = true)
	{
		$testArray = $this->test_list($testId);

		// Test Doesn't exist
		if ($testArray == null)
			return false;
		// not for this branch/year
		if ($testArray[0]->branch != $this->session->userdata('branch') || $testArray[0]->year != $this->session->userdata('year'))
			return false;
		// Test not started/expired
		if ($testArray[0]->date_start > time() || ($checkEndDate == true && $testArray[0]->date_end <= time()))
			return false;

		return true;
	}



	/**
	 * Time Remaining for Test(already Started)
	 * @method timeRemaining
	 * @param  int	$utId	Unique Test ID
	 * @return int			0 if time expired, -1 if not yet started, else, time remaining(in seconds)
	 */
	public function timeRemaining($utId)
	{
		$studentTestDetails = $this->getStudentTest($utId);
		if ($studentTestDetails != null) {
			if (time() >= $studentTestDetails[0]->time_end || $studentTestDetails[0]->status == 1) {
				return 0;
			}
			return ($studentTestDetails[0]->time_end-time());
		}
		return -1;

	}

	/**
	 * Saves the Answer to the database
	 * @method saveAnswer
	 * @param  int		$utId		Unique Test ID
	 * @param  int		$questionId	Question Number to be saved
	 * @param  string	$choice		Choice Marked
	 * @param  int		$status		1 = Marked, 0 = Unmarked
	 * @return boolean				true, if operation success, else false
	 */
	public function saveAnswer($utId, $questionId, $choice, $status)
	{
		// id, utest_id, question_id, choice, user_id
		// questions_attempted
		if ($status == 0) {
			// Delete
			$this->db->where('utest_id', $utId);
			$this->db->where('question_id', $questionId);
			$this->db->where('choice', $choice);
			$this->db->where('user_id', $this->session->userdata('id'));
			$this->db->delete('questions_attempted');
			$this->sql_log->info('Deleted QuestionId:'. ($questionId) .' Choice: '. ($choice) .' utId: '. ($utId), null, 'data_logs');
			return true;
		} else if ($status == 1) {
			// Insert
			$data = array(
				'utest_id' => $utId,
				'question_id' => $questionId,
				'choice' => $choice,
				'user_id' => $this->session->userdata('id'),
			);
			$this->sql_log->info('Inserted QuestionId:'. ($questionId) .' Choice: '. ($choice) .' utId: '. ($utId), null, 'data_logs');
			// Insert
			if ($this->db->insert('questions_attempted', $data)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get's Answers of all questions of specific test Id
	 * @method getAnswers
	 * @param  int	$utId	Unique Test ID
	 * @return array		Answer Array
	 */
	public function getAnswers($utId, $userId = null)
	{
		if ($userId == null) {
			$userId = $this->session->userdata('id');
		}
		$this->db->select('question_id, choice')
				->from('questions_attempted')
				->where('utest_id', $utId)
				->where('user_id', $userId);
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			$rw = $query->result();
			$answer_array = [];
			for ($i = 0; $i < count($rw); $i++) {
				$qNo = intval($rw[$i]->question_id);
				if (!isset($answer_array[$qNo])) {
					$answer_array[$qNo] = [];
				}
				$answer_array[$qNo][] = intval($rw[$i]->choice);

			}
			return $answer_array;
		}
		return [];
	}

	/**
	 * Gets Question Statistics(Questions Attempted)
	 * @method getQuestionStatistics
	 * @param  int		$utId	Unique Test ID
	 * @return array			Questions Array
	 */
	public function getQuestionStatistics($utId)
	{
		$this->db->select('question_id')
				->from('questions_attempted')
				->where('utest_id', $utId)
				->where('user_id', $this->session->userdata('id'))
				->group_by('question_id');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			$rw = $query->result();
			$questions_array = [];
			for ($i = 0; $i < count($rw); $i++) {
				$questions_array[] = intval($rw[$i]->question_id);
			}
			return $questions_array;
		}
		return [];
	}

	/**
	 * Checks if Test has been started by Student or not
	 * @method check_started_test
	 * @param  int		$testId	Test ID
	 * @return array			sql row
	 */
	public function check_started_test($testId)
	{
		$this->db->select('id, time_end')
				->from('test_student')
				->where('test_id', $testId)
				->where('user_id', $this->session->userdata('id'))
				->where('status', '0')
				->order_by('time_start', 'DESC');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			return $query->result();
		}
		return null;
	}

	/**
	 * Returns all test given by user.
	 * @method check_all_tests
	 * @param  int		$testId	Test ID
	 * @return array			sql row
	 */
	public function check_all_tests($testId)
	{
		$this->db->from('test_student')
				->where('test_id', $testId)
				->where('user_id', $this->session->userdata('id'))
				->order_by('time_start', 'DESC');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			return $query->result();
		}
		return null;
	}

	/**
	 * Returns all test given by any user
	 * @method check_all_tests_student.
	 * @param  int		$testId	Test ID
	 * @return array			sql row
	 */
	public function check_all_tests_student($testId)
	{
		$this->db->select('test_student.*, CONCAT(users.fname, \' \', users.lname) AS student_name, users.branch, users.year, users.uin')
				->from('test_student')
				->where('test_id', $testId)
				->join('users', 'users.id = test_student.user_id')
				->order_by('time_start', 'DESC');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			return $query->result();
		}
		return null;
	}

	/**
	 * Gets All Student Test of given UniqueID
	 * @method getStudentTest
	 * @param  int		$utId	UniqueTestId
	 * @return array			sql row
	 */
	public function getStudentTest($utId)
	{
		$this->db->from('test_student')
				->where('id', $utId)
				->order_by('time_start', 'DESC');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			return $query->result();
		}
		return null;
	}

	/**
	 * Starts new test
	 * @method start_test
	 * @param  int		$testId		Test ID
	 * @param  boolean	$newTest	true, if new test, false, if retake
	 * @return associative_array	[0] => Time to End(timestamp), [1] => Unique Test ID
	 */
	public function start_test($testId, $newTest)
	{
		$testArray = $this->test_list($testId);
		if ($testArray == null)
			return 0;

		$currentTime = time();

		$testDetails = $this->check_started_test($testId);
		if ($testDetails != null) {
			// Force to take old test first.
			if ($currentTime < $testDetails[0]->time_end)
				return ['end' => $testDetails[0]->time_end, 'id' => $testDetails[0]->id, 'status' => 2];
			// New Test = Don't allow retakes
			if ($newTest) {
				return ['status' => 0];
			}
		}

		$data = array(
			'test_id' => $testId,
			'user_id' => $this->session->userdata('id'),
			'time_start' => $currentTime,
			'time_end' => $currentTime + ($testArray[0]->time * 60),
			'score' => -1,
		);
		// Insert
		if ($this->db->insert('test_student', $data)) {
			$id = $this->db->insert_id();
			$this->sql_log->info('Test #'. $testId .' Started');
			return ['status' => 1, 'id' => $id];
		}
	}

	/**
	 * Ends Specific Test
	 * @method end_test
	 * @param  int	$utId	unique test ID
	 * @return bool			true, if successfully finished
	 */
	public function end_test($utId)
	{
		$testId = $this->input->post('test_id');
		$details = $this->check_started_test($testId);

		$data = array(
			'status' => '1',
		);

		// Update
		$this->db->where('id', $utId);
		$this->db->where('user_id', $this->session->userdata('id'));
		$this->db->update('test_student', $data);
		if ($this->db->affected_rows()) {
			$this->sql_log->info('Test '. $testId .', uTestId:'. $utId .' Completed');
			if ($this->generateScore($testId, $utId, $this->session->userdata('id')) != null)
				return true;
		}
		return false;
	}

	/**
	 * Generates Score for Specific Test of user
	 * @method generateScore
	 * @param  int		$testId	Test ID
	 * @param  int		$utId	Unique Test Id
	 * @param  int      $userId User ID
	 * @return array			null if failed, else true and score in array
	 */
	public function generateScore($testId, $utId, $userId = null)
	{
		$questionsArray = $this->question_list($testId);
		$answerArray = $this->getAnswers($utId, $userId);
		$score = 0;
		for ($i = 0; $i < count($questionsArray); $i++) {
			$id = intval($questionsArray[$i]->id);
			if (isset($answerArray[$id])) {
				$correct = intval($questionsArray[$i]->correct);
				$addScore = true;
				for ($j = 0; $j < count($answerArray[$id]); $j++) {
					$bitCheck = (1<<($answerArray[$id][$j]-1));
					if (($correct&$bitCheck) != $bitCheck) {
						$addScore = false;
						break;
					}
				}
				if ($addScore == true) {
					$score++;
				}
			}
		}
		$data = array(
			'score' => $score,
			'status' => 1,
		);

		$this->db->where('id', $utId)
				 ->group_start()
				 ->where('score', '-1')
				 ->or_where('status', '0')
				 ->group_end();
		if ($userId != null) {
			$this->db->where('user_id', $userId);
		} else {
			$this->db->where('user_id', $this->session->userdata('id'));
		}
		$this->db->update('test_student', $data);
		if ($this->db->affected_rows()) {
			if ($userId == null) {
				$this->sql_log->info('uTestID '. $utId .' Score Generated:'. $score);
			} else {
				// Admin Log
				$this->sql_log->info('uTestID '. $utId .' Score Generated:'. $score .' For User '. $userId);
				// Student Log
				$this->sql_log->info('uTestID '. $utId .' Score Generated:'. $score, $userId);
			}
			return [true, $score];
		}
		return null;
	}

	/**
	 * Lists Question Category
	 * @method listQuestionCategories
	 * @return array			sql row
	 */
	public function listQuestionCategories()
	{
		$this->db->select('CONCAT(category, \' (\', sub_category, \')\') AS name, id')
				->from('questions_main')
				->order_by('id', 'ASC');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			return $query->result();
		}
		return null;
	}

	/**
	 * Gets the Questions from category
	 * @method getQuestionCategory
	 * @param  int		$categoryId	Question Category Id
	 * @param  int		$count		Count of Questions to return
	 * @return array				sql row
	 */
	public function getQuestionCategory($categoryId, $count = 1)
	{
		$this->db->select('id, cat_id, question, answer AS correct, a, b, c, d, e, f')
				->from('questions_sub')
				->where('cat_id', $categoryId)
				->limit($count)
				->order_by('id', 'random');
		$query = $this->db->get();
		if ($query->result_id->num_rows > 0) {
			return $query->result();
		}
		return null;
	}

	/**
	 * Adds Question from category to TestId
	 * @method addQuestionCategory
	 * @param  int		$testId		TestId
	 * @param  array	$questions	Array Containing Questions object(sql row)
	 * @return array	[-1, null] if failed, else [DuplicateQuestions, QuestionsIdArray]
	 */
	public function addQuestionCategory($testId, $questions)
	{
		if (!$this->exists($testId))
			return [-1, null];
		if ($questions == null)
			return [-1, null];

		$error = 0;
		$que = [];
		for ($i = 0; $i < count($questions); $i++) {
			// Check Data First
			$this->db->from('test_questions')
					 ->where('test_id', $testId)
					 ->where('cat_id', $questions[$i]->cat_id)
					 ->where('category_question_id', $questions[$i]->id);
			$query = $this->db->get();
			if ($query->result_id->num_rows > 0) {
				$error++;
				$que[] = $questions[$i]->id;
				continue;
			}
			// Data to insert
			$data = array(
				'test_id' => $testId,
				'question' => $questions[$i]->question,
				'a' => $questions[$i]->a,
				'b' => $questions[$i]->b,
				'c' => $questions[$i]->c,
				'd' => $questions[$i]->d,
				'e' => $questions[$i]->e,
				'f' => $questions[$i]->f,
				'correct' => $questions[$i]->correct,
				'cat_id' => $questions[$i]->cat_id,
				'category_question_id' => $questions[$i]->id,
			);
			 // Insert
			if ($this->db->insert('test_questions', $data)) {
				$id = $this->db->insert_id();
				$this->sql_log->info('Question '. $id .' in Test#'. $testId .' Added by '. $this->session->userdata('name'));
			}
		}
		return [$error, $que];
	}

}
?>