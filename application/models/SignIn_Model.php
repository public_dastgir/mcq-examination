<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignIn_Model extends CI_Model {
	/**
	 * Processes SignIn Request and sets the session data.
	 * @method validate
	 * @return int			0, if fail, 1 if succeed, >1 if not verified
	 */
	public function validate()
	{
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$this->db->where('username', $username);
		
		$query = $this->db->get('users');

		if ($query->result_id->num_rows == 1) {
			$row = $query->row();
			if (password_verify($password, $row->password)) {
				$data = array(
							'id' => $row->id,
							'username' => $row->username,
							'branch' => $row->branch,
							'year' => $row->year,
							'validated' => true,
							'role' => $row->role,
							'uin' => $row->uin,
							'fname' => $row->fname,
							'lname' => $row->lname,
							'name' => $row->fname .' '. $row->lname,
						);
				if ($row->verified == 1) {
					$this->session->set_userdata($data);
					$this->sql_log->info('Logged In Successfully');
					return 1;
				}
				return $row->id+1;
			}
		}
		
		return 0;
	}

	/**
	 * Generates Random String
	 * @method generateRandomString
	 * @param  int		$length	Length of String to generate
	 * @return string			Random String
	 */
	private function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/**
	 * Sends Verification Email with Token to user.
	 * @method sendVerificationEmail
	 * @param  int		$userId	UserId
	 * @param  string	$email	Email Address
	 * @param  string	$token	Verification Token
	 * @return boolean			true, if email sent, else false
	 */
	public function sendVerificationEmail($userId, $email, $token)
	{
		$this->email->from('aptitude@dastgir.tech', 'Dastgir Pojee');
		$this->email->to($email);

		$this->email->subject('Verify Your Email Address');
		$this->email->message(
						"Hello User,\n".
						"\tPlease Verify your Email: ". $this->config->item('base_url') ."verify/". $userId ."/". $token ."/\n"
							);

		if ($this->email->send()) 
			return true;

		return false;
	}

	/**
	 * Updates Email Token(Used in resend email)
	 * @method updateEmailToken
	 * @param  int		$userId	UserId
	 * @param  string	$email	Email Address
	 * @return array			null, if not succeed, else [UserId, Token]
	 */
	public function updateEmailToken($userId, $email)
	{
		$token = $this->generateRandomString(20);

		if (!$this->sendVerificationEmail($userId, $email, $token))
			return NULL;

		$data = array(
			'token' => $token,
			'time' => time(),
		);
		// Update
		$this->db->where('id', $userId);
		$this->db->update('email_verify', $data);
		if ($this->db->affected_rows()) {
			return [$userId, $token];
		}
		return NULL;
	}

	/**
	 * Inserts Email Token (Used for Registration)
	 * @method insertEmailToken
	 * @param  int		$userId	UserId
	 * @param  string	$email	Email Address
	 * @return array			null, if not succeed, else [UserId, Token]
	 */
	public function insertEmailToken($userId, $email)
	{
		$token = $this->generateRandomString(20);
		if (!$this->sendVerificationEmail($userId, $email, $token))
			return NULL;

		$data = array(
			'id' => $userId,
			'token' => $token,
			'time' => time(),
		);
		if ($this->db->insert('email_verify', $data)) {
			return [$userId, $token];
		}
		return NULL;
	}

	/**
	 * Processes Registration Request
	 * @method register
	 * @return array			null if not succeed, else calls insertEmailToken()
	 */
	public function register()
	{
		$data = array(
			'username' => $this->input->post('uname'),
			'fname' => $this->input->post('fname'),
			'lname' => $this->input->post('lname'),
			'email' => $this->input->post('email'),
			'branch' => $this->input->post('branch'),
			'year' => $this->input->post('year'),
			'uin' => $this->input->post('uin'),
			'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT, ['cost' => '10']),
			'verified' => 0,
			'role' => 'S',
		);
			
		// Insert User and send email verification.
		if ($this->db->insert('users', $data)) {
			$user_id = $this->db->insert_id();
			return $this->insertEmailToken($user_id, $this->input->post('email'));
		}
		return null;
	}

	/**
	 * Verifies the Email
	 * @method checkEmail
	 * @param  int		$userId	UserId
	 * @param  string	$token	Verification Token
	 * @return bool				true, if verified, else false.
	 */
	public function checkEmail($userId, $token) {
		$this->db->where('id', $userId);
		$this->db->where('verified', '0');
		$query = $this->db->get('users');
		if ($query->result_id->num_rows == 1) {
			$this->db->where('id', $userId);
			$this->db->where('token', $token);
			$query = $this->db->get('email_verify');
			if ($query->result_id->num_rows == 1) {
				$this->db->set('verified', '1');
				$this->db->where('id', $userId);
				$this->db->update('users');
				return true;
			}
		}
		return false;
	}

	/**
	 * Resends the Verification Email
	 * @method resendEmail
	 * @param  int		$userId	UserId
	 * @param  string	$token	Username(bcrypt'd)
	 * @return int				1, if valid, else [0, -1, -2]
	 */
	public function resendEmail($userId, $token) {
		$this->db->where('id', $userId);
		$this->db->where('verified', '0');
		$query = $this->db->get('users');
		if ($query->result_id->num_rows == 1) {
			$row = $query->row();
			if (password_verify($row->username, $token)) {
				return -1;
			}
			$email = $row->email;
			$this->db->where('id', $userId);
			$query = $this->db->get('email_verify');
			switch ($query->result_id->num_rows) {
				case 1:
					$row = $query->row();
					if (time() < $row->time+24*60*60) {
						return -2;
					}
					if ($this->updateEmailToken($userId, $email) != null)
						return 1;
					break;
				case 0:
					if ($this->insertEmailToken($userId, $email) != null)
						return 1;
					break;
			}
		}
		return 0;
	}
}
?>