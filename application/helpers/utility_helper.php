<?php
/**
 * Gives the Asset Directory
 * @method asset_url
 * @return string	contains asset directory
 */
function asset_url()
{
	return base_url() .'assets/';
}
?>