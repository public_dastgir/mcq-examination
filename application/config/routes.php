<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'SignIn';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

// Sign In
$route['sign_in']['get'] = 'SignIn';
$route['sign_in']['post'] = 'SignIn/process';

// Register
$route['sign_in/register']['get'] = 'SignIn/register';
$route['sign_in/register']['post'] = 'SignIn/register_process';

// Verify Email
$route['verify/([0-9]+)/([A-Za-z0-9]+)']['get'] = 'SignIn/emailVerify/$1/$2';
$route['resend/([0-9]+)/([0-9A-Za-z]+)']['get'] = 'SignIn/emailResend/$1/$2';

// Register
$route['logout']['get'] = 'SignIn/do_logout';

/** Student Roles */
/** Profile */
// Home Page
$route['profile'] = 'Profile';
// Logs
$route['profile/logs'] = 'Profile/view_logs';

/** Tests */
// List
$route['test/list'] = 'StudentTest/list_tests/test';
// Take/Retake
$route['test/take/([0-9]+)'] = 'StudentTest/takeNew/$1';
$route['test/retake/([0-9]+)'] = 'StudentTest/retake/$1';
$route['test/answers/([0-9]+)/([0-9]+)'] = 'StudentTest/showAnswers/$1/$2';
// Get Questions/Answers
$route['test/getAnswers/([0-9]+)']['post'] = 'StudentTest/getAnswers/$1';
$route['test/saveAnswer/([0-9]+)']['post'] = 'StudentTest/saveAnswer/$1';
$route['test/getQuestionStatistics/([0-9]+)']['post'] = 'StudentTest/getQuestionStatistics/$1';
// End Test
$route['test/finish/([0-9]+)']['post'] = 'StudentTest/finishTest/$1';

// Scores
$route['test/listScores'] = 'StudentTest/list_tests/score';
$route['test/score/([0-9]+)']['post'] = 'StudentTest/getScore/$1';

/** Admin Roles */
$route['admin'] = 'Admin';

// Add Test
$route['admin/test'] = 'Test/add';
$route['admin/test/add']['get'] = 'Test/add';
$route['admin/test/add']['post'] = 'Test/add_new_test';
// Edit Test
$route['admin/test/edit/(error|[0-9]+)']['get'] = 'Test/edit/$1';
// List Test
$route['admin/test/list']['get'] = 'Test/list_tests';
$route['admin/test/remove/([0-9]+)']['post'] = 'Test/remove/$1';
// Settings
$route['admin/test/settings/([0-9]+)']['post'] = 'Test/settings/$1';
// Questions
$route['admin/test/add_question/([0-9]+)']['post'] = 'Test/add_new_question/$1';
$route['admin/test/remove_question/([0-9]+)']['post'] = 'Test/remove_question/$1';
$route['admin/test/add_question_category/([0-9]+)']['post'] = 'Test/add_new_question_category/$1';
// Statistics
$route['admin/test/stats/([0-9]+)']['get'] = 'Test/statistics/$1';
